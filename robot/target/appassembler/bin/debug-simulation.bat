@REM ----------------------------------------------------------------------------
@REM  Copyright 2001-2006 The Apache Software Foundation.
@REM
@REM  Licensed under the Apache License, Version 2.0 (the "License");
@REM  you may not use this file except in compliance with the License.
@REM  You may obtain a copy of the License at
@REM
@REM       http://www.apache.org/licenses/LICENSE-2.0
@REM
@REM  Unless required by applicable law or agreed to in writing, software
@REM  distributed under the License is distributed on an "AS IS" BASIS,
@REM  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@REM  See the License for the specific language governing permissions and
@REM  limitations under the License.
@REM ----------------------------------------------------------------------------
@REM
@REM   Copyright (c) 2001-2006 The Apache Software Foundation.  All rights
@REM   reserved.

@echo off

set ERROR_CODE=0

:init
@REM Decide how to startup depending on the version of windows

@REM -- Win98ME
if NOT "%OS%"=="Windows_NT" goto Win9xArg

@REM set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" @setlocal

@REM -- 4NT shell
if "%eval[2+2]" == "4" goto 4NTArgs

@REM -- Regular WinNT shell
set CMD_LINE_ARGS=%*
goto WinNTGetScriptDir

@REM The 4NT Shell from jp software
:4NTArgs
set CMD_LINE_ARGS=%$
goto WinNTGetScriptDir

:Win9xArg
@REM Slurp the command line arguments.  This loop allows for an unlimited number
@REM of arguments (up to the command line limit, anyway).
set CMD_LINE_ARGS=
:Win9xApp
if %1a==a goto Win9xGetScriptDir
set CMD_LINE_ARGS=%CMD_LINE_ARGS% %1
shift
goto Win9xApp

:Win9xGetScriptDir
set SAVEDIR=%CD%
%0\
cd %0\..\.. 
set BASEDIR=%CD%
cd %SAVEDIR%
set SAVE_DIR=
goto repoSetup

:WinNTGetScriptDir
for %%i in ("%~dp0..") do set "BASEDIR=%%~fi"

:repoSetup
set REPO=


if "%JAVACMD%"=="" set JAVACMD=java

if "%REPO%"=="" set REPO=%BASEDIR%\repo

set CLASSPATH="%BASEDIR%"\etc;"%REPO%"\org\tahomarobotics\sim-model\2020.0.1-SNAPSHOT\sim-model-2020.0.1-SNAPSHOT.jar;"%REPO%"\org\tahomarobotics\bear-simulation\2020.1.0-SNAPSHOT\bear-simulation-2020.1.0-SNAPSHOT.jar;"%REPO%"\org\javassist\javassist\3.26.0-GA\javassist-3.26.0-GA.jar;"%REPO%"\org\lwjgl\osgi\org.lwjgl.glfw\3.2.1.2\org.lwjgl.glfw-3.2.1.2.jar;"%REPO%"\org\lwjgl\osgi\org.lwjgl.lwjgl\3.2.1.2\org.lwjgl.lwjgl-3.2.1.2.jar;"%REPO%"\org\openjfx\javafx-controls\13.0.2\javafx-controls-13.0.2.jar;"%REPO%"\org\openjfx\javafx-controls\13.0.2\javafx-controls-13.0.2-win.jar;"%REPO%"\org\openjfx\javafx-graphics\13.0.2\javafx-graphics-13.0.2.jar;"%REPO%"\org\openjfx\javafx-graphics\13.0.2\javafx-graphics-13.0.2-win.jar;"%REPO%"\org\openjfx\javafx-base\13.0.2\javafx-base-13.0.2.jar;"%REPO%"\org\openjfx\javafx-base\13.0.2\javafx-base-13.0.2-win.jar;"%REPO%"\org\tahomarobotics\bear-essentials\2020.1.0-SNAPSHOT\bear-essentials-2020.1.0-SNAPSHOT.jar;"%REPO%"\com\fasterxml\jackson\dataformat\jackson-dataformat-smile\2.9.6\jackson-dataformat-smile-2.9.6.jar;"%REPO%"\com\ctre\phoenix\api-java\5.17.6\api-java-5.17.6.jar;"%REPO%"\edu\wpi\first\wpilibOldCommands\wpilibOldCommands-java\2020.2.2\wpilibOldCommands-java-2020.2.2.jar;"%REPO%"\edu\wpi\first\wpilibj\wpilibj-java\2020.2.2\wpilibj-java-2020.2.2.jar;"%REPO%"\edu\wpi\first\hal\hal-java\2020.2.2\hal-java-2020.2.2.jar;"%REPO%"\edu\wpi\first\hal\hal-jni\2020.2.2\hal-jni-2020.2.2-windowsx86-64.jar;"%REPO%"\edu\wpi\first\ntcore\ntcore-java\2020.2.2\ntcore-java-2020.2.2.jar;"%REPO%"\edu\wpi\first\ntcore\ntcore-jni\2020.2.2\ntcore-jni-2020.2.2-windowsx86-64.jar;"%REPO%"\edu\wpi\first\cameraserver\cameraserver-java\2020.2.2\cameraserver-java-2020.2.2.jar;"%REPO%"\edu\wpi\first\cscore\cscore-java\2020.2.2\cscore-java-2020.2.2.jar;"%REPO%"\edu\wpi\first\cscore\cscore-jni\2020.2.2\cscore-jni-2020.2.2-windowsx86-64.jar;"%REPO%"\edu\wpi\first\wpiutil\wpiutil-java\2020.2.2\wpiutil-java-2020.2.2.jar;"%REPO%"\org\apache\logging\log4j\log4j-slf4j-impl\2.11.1\log4j-slf4j-impl-2.11.1.jar;"%REPO%"\org\slf4j\slf4j-api\1.7.25\slf4j-api-1.7.25.jar;"%REPO%"\org\apache\logging\log4j\log4j-api\2.11.1\log4j-api-2.11.1.jar;"%REPO%"\org\apache\logging\log4j\log4j-core\2.11.1\log4j-core-2.11.1.jar;"%REPO%"\com\revrobotics\frc\SparkMax-java\1.5.1\SparkMax-java-1.5.1.jar;"%REPO%"\com\fasterxml\jackson\core\jackson-databind\2.9.6\jackson-databind-2.9.6.jar;"%REPO%"\com\fasterxml\jackson\core\jackson-annotations\2.9.0\jackson-annotations-2.9.0.jar;"%REPO%"\com\fasterxml\jackson\core\jackson-core\2.9.6\jackson-core-2.9.6.jar;"%REPO%"\org\tahomarobotics\robot\2020.0.1-SNAPSHOT\robot-2020.0.1-SNAPSHOT.jar

set ENDORSED_DIR=
if NOT "%ENDORSED_DIR%" == "" set CLASSPATH="%BASEDIR%"\%ENDORSED_DIR%\*;%CLASSPATH%

if NOT "%CLASSPATH_PREFIX%" == "" set CLASSPATH=%CLASSPATH_PREFIX%;%CLASSPATH%

@REM Reaching here means variables are defined and arguments have been captured
:endInit

%JAVACMD% %JAVA_OPTS% -javaagent:C:\Users\Robert\.m2\repository\org\tahomarobotics\bear-simulation\2020.1.0-SNAPSHOT\bear-simulation-2020.1.0-SNAPSHOT.jar -Dsimulation.models=org.tahomarobotics.simulation.Model -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=8000 -classpath %CLASSPATH% -Dapp.name="debug-simulation" -Dapp.repo="%REPO%" -Dapp.home="%BASEDIR%" -Dbasedir="%BASEDIR%" org.tahomarobotics.robot.Robot %CMD_LINE_ARGS%
if %ERRORLEVEL% NEQ 0 goto error
goto end

:error
if "%OS%"=="Windows_NT" @endlocal
set ERROR_CODE=%ERRORLEVEL%

:end
@REM set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" goto endNT

@REM For old DOS remove the set variables from ENV - we assume they were not set
@REM before we started - at least we don't leave any baggage around
set CMD_LINE_ARGS=
goto postExec

:endNT
@REM If error code is set to 1 then the endlocal was done already in :error.
if %ERROR_CODE% EQU 0 @endlocal


:postExec

if "%FORCE_EXIT_ON_ERROR%" == "on" (
  if %ERROR_CODE% NEQ 0 exit %ERROR_CODE%
)

exit /B %ERROR_CODE%

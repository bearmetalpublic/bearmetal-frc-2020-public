package org.tahomarobotics.robot.queuer;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.UpdateIF;
import org.tahomarobotics.robot.util.LiveChartData;
import org.tahomarobotics.robot.util.GenericMotor;
import org.tahomarobotics.robot.util.GenericMotorBuilder;
import org.tahomarobotics.robot.util.MotorInfo;

import java.util.ArrayList;
import java.util.List;

public class Queuer extends Subsystem implements UpdateIF, MotorInfo.SubsystemMotors {

    private static final Queuer INSTANCE = new Queuer();
    private static final double SONIC_THRESHOLD = 1.8;

    private CANSparkMax trashCompactor = new CANSparkMax(RobotMap.TRASH_COMPACTOR, CANSparkMaxLowLevel.MotorType.kBrushless);
    private CANSparkMax motorConveyor = new CANSparkMax(RobotMap.QUEUER_MOTOR_CONVEYOR, CANSparkMaxLowLevel.MotorType.kBrushless);
    private AnalogInput sonicSensorFirst = new AnalogInput(RobotMap.QUEUER_SONIC_SENSOR_FIRST);
    private AnalogInput sonicSensorSecond = new AnalogInput(RobotMap.QUEUER_SONIC_SENSOR_SECOND);

    private final ArrayList<GenericMotor> motors = GenericMotorBuilder.convertMotorList(new CANSparkMax[]{motorConveyor, trashCompactor});

    private QueuerState state = QueuerState.Active;

    public enum QueuerState{
        Active,
        Eject,
        ScrewTheSensors;

    }

    private Queuer(){
        motorConveyor.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus0, 20);
        motorConveyor.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus1, 20);
        motorConveyor.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus2, 20);

        trashCompactor.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus0, 20);
        trashCompactor.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus1, 20);
        trashCompactor.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus2, 20);

        trashCompactor.setInverted(true);
        motorConveyor.setIdleMode(CANSparkMax.IdleMode.kBrake);
    }

    private void trash(double power){
        trashCompactor.set(power);
    }
    private boolean stuck = false;
    public void setStuck(boolean stuck){
        this.stuck = stuck;
    }
    private LiveChartData chartData = new LiveChartData("Queuer"," Time (Sec)", "Degrees",
            new String[]{"Bottom-sensor"});
    public void update(boolean enabled){
        chartData.updateData(new double[]{Timer.getFPGATimestamp(), sonicSensorFirst.getVoltage()});
        if(stuck){
            trash(-.5);
        }else if(isBallAtEnd()){
            trash(0.0);
        }else{
            trash(.8);
        }
        SmartDashboard.putNumber("speed", getVelocity());
        SmartDashboard.putString("state", state.toString());
        SmartDashboard.putBoolean("is ball ready", isBallReady());
        SmartDashboard.putBoolean("yeet", trashCompactor.isFollower());
    }

    public void setState(QueuerState state){
        this.state = state;
    }

    public static Queuer getInstance(){
        return INSTANCE;
    }

    public void setQueuerPower(double power){
        if(!isBallAtEnd() || state == QueuerState.Eject){
            SmartDashboard.putNumber("power", power);
            motorConveyor.set(power);
        }else {
            motorConveyor.set(0.0);
        }
    }

    public boolean isBallReady(){
        return sonicSensorFirst.getVoltage() < SONIC_THRESHOLD;
    }
    public boolean isBallAtEnd(){
        return sonicSensorSecond.getVoltage() < SONIC_THRESHOLD;
    }
    public double getVelocity(){
        return motorConveyor.getEncoder().getVelocity() * QueuerConstants.ROTATION_TO_INCHES;
    }

    @Override
    protected void initDefaultCommand() {
        setDefaultCommand(new MoveBall());
    }

    @Override
    public List<GenericMotor> getMotorInfo() {
        return motors;
    }

    @Override
    public String getSubsystemName() {
        return Queuer.class.getSimpleName();
    }

}

package org.tahomarobotics.robot.queuer;

import edu.wpi.first.wpilibj.command.Command;

public class Eject extends Command {

    private final Queuer queuer = Queuer.getInstance();

    private double timeout;
    public Eject(){
        this(2.0);
    }
    public Eject(double timeout){
        requires(queuer);
        this.timeout = timeout;
    }

    @Override
    protected void initialize() {
        System.out.println("ejecting");
        queuer.setState(Queuer.QueuerState.Eject);
        setTimeout(timeout);
    }

    @Override
    protected boolean isFinished() {

        queuer.setQueuerPower(1.);

        return isTimedOut();
    }

    @Override
    protected void end() {
        queuer.setState(Queuer.QueuerState.Active);
        queuer.setQueuerPower(0.0);
    }
}

package org.tahomarobotics.robot.queuer;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfileFactory;
import org.tahomarobotics.robot.motion.MotionState;

public class MoveBall extends Command {

    private final Queuer queuer = Queuer.getInstance();

    private final double voltageOutput = 3;

    public MoveBall(){
        requires(queuer);
    }

    double initTime = Double.POSITIVE_INFINITY;
    double restTime = .01;
    @Override
    protected void initialize() {
        queuer.setState(Queuer.QueuerState.Active);
    }

    @Override
    protected boolean isFinished() {
        if(queuer.isBallReady() && initTime == Double.POSITIVE_INFINITY){
            initTime = Timer.getFPGATimestamp();
        }else if(!queuer.isBallReady()){
            initTime = Double.POSITIVE_INFINITY;
        }
        if(Timer.getFPGATimestamp() - initTime > restTime){
            queuer.setQueuerPower(voltageOutput / RobotController.getBatteryVoltage());
        }else {
            queuer.setQueuerPower(0.0);
        }
        return false;
    }

    @Override
    protected void end() {
        queuer.setQueuerPower(0.0);
        queuer.setState(Queuer.QueuerState.Eject);
    }
}

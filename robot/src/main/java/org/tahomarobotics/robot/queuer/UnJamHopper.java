package org.tahomarobotics.robot.queuer;

import edu.wpi.first.wpilibj.command.Command;

public class UnJamHopper extends Command {

    private final Queuer queuer = Queuer.getInstance();

    public UnJamHopper(double timeout){
        setTimeout(timeout);
    }
    @Override
    protected boolean isFinished() {
        queuer.setStuck(true);
        return isTimedOut();
    }

    @Override
    protected void end() {
        queuer.setStuck(false);
    }
}

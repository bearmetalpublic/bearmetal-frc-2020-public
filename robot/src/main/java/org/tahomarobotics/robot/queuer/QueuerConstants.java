package org.tahomarobotics.robot.queuer;

public class QueuerConstants {
    public static final double ROTATION_TO_INCHES = .8; // TODO get number of encoder ticks per inch of conveyor
    public static final double WANTED_VEL = 100; // in/s
}

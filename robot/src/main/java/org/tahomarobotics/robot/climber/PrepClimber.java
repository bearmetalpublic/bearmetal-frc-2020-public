package org.tahomarobotics.robot.climber;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;
import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.auto.commands.TurretGood;
import org.tahomarobotics.robot.turret.TurnToAngle;
import org.tahomarobotics.robot.collector.CollectorSolenoidCommand;

public class PrepClimber extends CommandGroup {
	public PrepClimber(){
		addSequential(new Command() {
			@Override
			protected boolean isFinished() {
				OI.getInstance().setClimbPrepped(true);
				return true;
			}
		});
		addSequential(new TurnToAngle(-100, true, -1));
		addSequential(new TurretGood());
		addSequential(new ClimberSolenoidCommand(true));
	}
}

package org.tahomarobotics.robot.climber;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.UpdateIF;

public class Climber extends Subsystem implements UpdateIF {
    private static final Climber INSTANCE = new Climber();

    private final TalonFX climberMotor = new TalonFX(RobotMap.CLIMBER_MOTOR);
    private final TalonFX climberMotor1 = new TalonFX(RobotMap.CLIMBER_MOTOR_1);

    private final Solenoid breakSolenoid = new Solenoid(RobotMap.CLIMBER_SOLENOID);

    public static Climber getInstance(){
        return INSTANCE;
    }

    private Climber(){

        climberMotor.getStatusFramePeriod(StatusFrame.Status_2_Feedback0, 20);
        climberMotor1.getStatusFramePeriod(StatusFrame.Status_2_Feedback0, 20);

        climberMotor.setNeutralMode(NeutralMode.Coast);
        climberMotor1.setNeutralMode(NeutralMode.Coast);
        climberMotor1.follow(climberMotor);
        climberMotor.setInverted(true);
        climberMotor1.setInverted(true);
    }

    public void setPower(double motorPower){
        climberMotor.set(TalonFXControlMode.PercentOutput, motorPower);
    }

    public void solenoidMovement(boolean movement){
        breakSolenoid.set(movement);
    }

    @Override
    protected void initDefaultCommand(){
        setDefaultCommand(new ClimberCommand());
    }

    @Override
    public void update(boolean isEnabled) {

    }

}

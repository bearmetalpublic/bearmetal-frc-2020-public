package org.tahomarobotics.robot.climber;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Command;

public class ClimberSolenoidCommand extends Command {

    private boolean engaged;

    private final Climber climber = Climber.getInstance();

    public ClimberSolenoidCommand(boolean engaged){
        this.engaged = engaged;
    }

    @Override
    protected boolean isFinished() {
        climber.solenoidMovement(engaged);
        return true;
    }
}

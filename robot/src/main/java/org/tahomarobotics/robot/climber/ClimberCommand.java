package org.tahomarobotics.robot.climber;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.OI;

public class ClimberCommand extends Command {
    private final Climber climber = Climber.getInstance();
    private final OI oi = OI.getInstance();

    ClimberCommand(){
        requires(climber);
    }

    @Override
    protected boolean isFinished() {
        double manip = 0.0;
        double driv = 0.0;
        if(oi.isClimbPrepped()) {
            manip = (oi.getRightManipThrottle()) / 2.0;
            driv = (oi.getDriverLeftTrigger() - oi.getDriverRightTrigger()) / 2.0;
        }
        climber.setPower(Math.abs(manip) > Math.abs(driv) ? manip : driv);
        return true;
    }
}
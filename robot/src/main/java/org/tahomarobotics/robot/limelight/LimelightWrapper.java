package org.tahomarobotics.robot.limelight;


import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.InitIF;
import org.tahomarobotics.robot.UpdateIF;

public class LimelightWrapper implements UpdateIF, InitIF {

    private static final LimelightWrapper INSTANCE = new LimelightWrapper();
    
    public static LimelightWrapper getInstance(){
        return INSTANCE;
    }
    private NetworkTable table = NetworkTableInstance.getDefault().getTable("limelight");
    //x location of the target
    private NetworkTableEntry tx = table.getEntry("tx");
    //y location of the target
    private NetworkTableEntry ty = table.getEntry("ty");
    //area of the target
    private NetworkTableEntry ta = table.getEntry("ta");
    //does the limelight have a target
    private NetworkTableEntry tv = table.getEntry("tv");

    private NetworkTableEntry ts = table.getEntry("ts");

    private NetworkTableEntry thor = table.getEntry("thor");
    private NetworkTableEntry tvert = table.getEntry("tvert");

    private NetworkTableEntry tshort = table.getEntry("tshort");
    private NetworkTableEntry tlong = table.getEntry("tlong");

    public double getX(){
        return tx.getDouble(0.0);
    }

    public double getY(){
        return ty.getDouble(0.0);
    }

    private double b = 32.19209159;
    private double a = 259.7007949;
    public double getDistanceSkew(){
        return getDistance(true);
    }
    public double getDistance(boolean area){
        if(area){
//            return -26.86091 + (29790640 + 26.86091)/(1 + Math.pow(getArea()/(8.337552*Math.pow(10,-7)),0.506215));
            return  (-70.35297 + (37212010 + 70.35297)/(1 + Math.pow(getArea()/(3.395209*Math.pow(10,-13)), 0.4111744))) * 1.1;
        }else {
            double heightOfCamera = 39;//TODO:update this number
            double heightOfTarget = 88.25 - (15 / 2.0);
            double angleOfCamera = 22;//TODO: update this number
            double angleofTarget = ty.getDouble(0.0);

            SmartDashboard.putNumber("angle", angleofTarget);
            return (heightOfTarget - heightOfCamera) / Math.tan(Math.toRadians(angleOfCamera + angleofTarget));
        }
    }

    public double getAreaVert(){
        return thor.getDouble(0.0) * tvert.getDouble(0.0);
    }
    public double getAreaShortLong(){
        return tshort.getDouble(0.0) * tlong.getDouble(0.0);
    }
    public double getArea(){
//        return thor.getDouble(0.0) * tvert.getDouble(0.0);
        return ta.getDouble(0.0);
    }

    public double getSkew(){
        return ts.getDouble(0.0);
    }
    public boolean isTargetAvalible(){
        return getArea() != 0.0;
    }

    //Modeing the limelight to work for the individual case
    public void drivingMode(boolean driveMode){
        ledMode(!driveMode);
        double mode = driveMode ? 1 : 0;
        table.getEntry("camMode").setNumber(mode);
    }

    public void ledMode(boolean on){
        double mode = on ? 0 : 1;
        table.getEntry("ledMode").setNumber(mode);
    }

    public String getVisionMode(){
        return  table.getEntry("getpipe").getString("0");
    }

    @Override
    public void update(boolean isEnabled) {
        SmartDashboard.putNumber("limelight x", getX());
        SmartDashboard.putNumber("limelight y", getY());
        SmartDashboard.putNumber("limelight area", getArea());
        SmartDashboard.putNumber("limelight distance area", getDistance(true));
        SmartDashboard.putNumber("limelight distance angle", getDistance(false));
        SmartDashboard.putBoolean("limelight has target", isTargetAvalible());
        SmartDashboard.putString("limelight mode", getVisionMode());
        SmartDashboard.putNumber("limelight skew", getSkew());
        SmartDashboard.putNumber("Limelight distance skew", getDistanceSkew());
    }

    @Override
    public void init() {
        ledMode(true);
    }
}

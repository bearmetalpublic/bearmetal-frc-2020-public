package org.tahomarobotics.robot.collector;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.path.ActionIF;

public class CollectorSolenoidCommand extends Command implements ActionIF {

    private final DoubleSolenoid.Value engaged;

    private final Collector collector = Collector.getInstance();

    public CollectorSolenoidCommand(DoubleSolenoid.Value engaged){
        this.engaged = engaged;
    }


    @Override
    protected boolean isFinished() {
        collector.movement(engaged);
        return true;
    }
}

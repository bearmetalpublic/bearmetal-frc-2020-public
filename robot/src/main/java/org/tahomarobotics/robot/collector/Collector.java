package org.tahomarobotics.robot.collector;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import org.tahomarobotics.robot.DisabledInitIF;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.UpdateIF;
import org.tahomarobotics.robot.util.GenericMotor;
import org.tahomarobotics.robot.util.GenericMotorBuilder;
import org.tahomarobotics.robot.util.MotorInfo;

import java.util.ArrayList;
import java.util.List;

public class Collector extends Subsystem implements UpdateIF, DisabledInitIF, MotorInfo.SubsystemMotors {
    private static final Collector INSTANCE = new Collector();

    private final TalonFX collectorMotorFront = new TalonFX(RobotMap.COLLECTOR_MOTOR_FRONT);

    private final DoubleSolenoid extender = new DoubleSolenoid(RobotMap.COLLECTOR_SOLENOID, 1);

    private final ArrayList<GenericMotor> motors = GenericMotorBuilder.convertMotorList(new TalonFX[]{collectorMotorFront});

    public static Collector getInstance(){
        return INSTANCE;
    }

    private Collector(){
        collectorMotorFront.setStatusFramePeriod(StatusFrame.Status_2_Feedback0, 20);
        collectorMotorFront.setNeutralMode(NeutralMode.Coast);
    }

    public void setPower(double motorPower){
        collectorMotorFront.set(TalonFXControlMode.PercentOutput, motorPower);
    }

    public void movement(DoubleSolenoid.Value movement){
        extender.set(movement);
    }

    @Override
    protected void initDefaultCommand(){
        setDefaultCommand(new CollectorCommand());
    }

    @Override
    public void update(boolean isEnabled) {

    }

    @Override
    public void disabledInit() {
    }

    @Override
    public List<GenericMotor> getMotorInfo() {
        return motors;
    }

    @Override
    public String getSubsystemName() {
        return Collector.class.getSimpleName();
    }
}

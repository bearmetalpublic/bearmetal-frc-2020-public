package org.tahomarobotics.robot.collector;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.OI;

public class CollectorCommand extends Command {
    private final Collector collector = Collector.getInstance();
    private final OI oi = OI.getInstance();

    CollectorCommand(){
        requires(collector);
    }

    @Override
    protected boolean isFinished() {
        if(!oi.isClimbPrepped()) {
            double driver = (oi.getDriverLeftTrigger() - oi.getDriverRightTrigger()) / 2.0;
            double manip = (oi.getManipLeftTrigger() - oi.getManipRightTrigger()) / 2.0;
            collector.setPower(Math.abs(driver) > Math.abs(manip) ? driver : manip);
        } else {
            collector.setPower(0.0);
        }
        return true;
    }
}
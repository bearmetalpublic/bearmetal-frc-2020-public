package org.tahomarobotics.robot.collector;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.path.ActionIF;

public class CollectorAutoCommand extends Command implements ActionIF {

	private final Collector collector = Collector.getInstance();

	public CollectorAutoCommand(double timeout){
		setTimeout(timeout);
		requires(collector);
	}

	@Override
	protected void initialize() {
		collector.setPower(1.0);
	}

	@Override
	protected boolean isFinished() {
		return isTimedOut();
	}

	@Override
	protected void end() {
		collector.setPower(0.0);
	}
}

/*
 * Copyright 2020 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.chassis;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import org.tahomarobotics.robot.DisabledInitIF;
import org.tahomarobotics.robot.InitIF;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.UpdateIF;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.util.GenericMotor;
import org.tahomarobotics.robot.util.GenericMotorBuilder;
import org.tahomarobotics.robot.util.LiveChartData;
import org.tahomarobotics.robot.util.MotorInfo;

import java.util.List;

public class Chassis extends Subsystem implements UpdateIF, InitIF, DisabledInitIF, MotorInfo.SubsystemMotors {

    private static final Chassis INSTANCE = new Chassis();
    private final TalonFX leftFrontMaster = new TalonFX(RobotMap.LEFT_FRONT_MOTOR);
    private final TalonFX leftBackFollower = new TalonFX(RobotMap.LEFT_REAR_MOTOR);
    private final TalonFX rightFrontMaster = new TalonFX(RobotMap.RIGHT_FRONT_MOTOR);
    private final TalonFX rightBackFollower = new TalonFX(RobotMap.RIGHT_REAR_MOTOR);

    private final List<GenericMotor> motors = GenericMotorBuilder.convertMotorList
            (new TalonFX[] { leftFrontMaster, leftBackFollower, rightFrontMaster, rightBackFollower} );

    private double offset;
    private Chassis(){
        rightFrontMaster.setNeutralMode(NeutralMode.Coast);
        rightBackFollower.setNeutralMode(NeutralMode.Coast);
        leftFrontMaster.setNeutralMode(NeutralMode.Coast);
        leftBackFollower.setNeutralMode(NeutralMode.Coast);

        rightFrontMaster.getStatusFramePeriod(StatusFrame.Status_2_Feedback0, 20);
        rightBackFollower.getStatusFramePeriod(StatusFrame.Status_2_Feedback0, 20);
        leftFrontMaster.getStatusFramePeriod(StatusFrame.Status_2_Feedback0, 20);
        leftBackFollower.getStatusFramePeriod(StatusFrame.Status_2_Feedback0, 20);

        rightFrontMaster.setInverted(true);
        rightBackFollower.setInverted(true);

        leftBackFollower.follow(leftFrontMaster);
        rightBackFollower.follow(rightFrontMaster);

        offset = getDistance();
    }

    public static Chassis getInstance() {
        return INSTANCE;
    }

    public void setBrakeMode(NeutralMode mode){
        rightFrontMaster.setNeutralMode(mode);
        rightBackFollower.setNeutralMode(mode);
        leftFrontMaster.setNeutralMode(mode);
        leftBackFollower.setNeutralMode(mode);
    }

    @Override
    protected void initDefaultCommand() {
        setDefaultCommand(new TeleopDrive());
    }

	public double getLeftVel(){
		return leftFrontMaster.getSelectedSensorVelocity() * ChassisConstants.INCHES_PER_SECOND;
	}
	public double getRightVel(){
		return rightFrontMaster.getSelectedSensorVelocity() * ChassisConstants.INCHES_PER_SECOND;
	}

	public double getDistance(){
        return (leftFrontMaster.getSelectedSensorPosition() * ChassisConstants.INCHES_PER_TICK) - offset;
    }
	public double getForwardVel(){
    	return (getLeftVel() + getRightVel()) / 2.0;
	}
	public double getRotational(){
        return  (getRightVel() - getLeftVel()) / 2.0;
    }
    public void setPower(double forwardPower, double rotatePower) {
        setLeftRightPower(forwardPower - rotatePower, forwardPower + rotatePower);
    }

    public void setLeftRightPower(double left, double right) {
        leftFrontMaster.set(ControlMode.PercentOutput, left);
        rightFrontMaster.set(ControlMode.PercentOutput, right);
    }
    double wanted = 0.0;
    private final LiveChartData chartData = new LiveChartData("chassis", "time", "vel", new String[]{"rotational", "wanted"});
    public void update(boolean enabled){
        chartData.updateData(new double[]{Timer.getFPGATimestamp(), RobotState.getInstance().getRotVel(), wanted});
    }

    @Override
    public void disabledInit() {
        setBrakeMode(NeutralMode.Coast);
    }

    @Override
    public void init() {
        setBrakeMode(NeutralMode.Brake);
    }

//    @Override
//    public ArrayList<TalonFX> getMotorInfo() {
//        return motors;
//    }
    @Override
    public List<GenericMotor> getMotorInfo() {
        return motors;
    }

    @Override
    public String getSubsystemName() {
        return Chassis.class.getSimpleName();
    }
}

/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.chassis;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.motion.Motion2DProfileFactory;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionProfiles;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.path.AdaptivePurePursuitController;
import org.tahomarobotics.robot.path.PathActions;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.path.PathBuilder.Mirror;
import org.tahomarobotics.robot.path.PathBuilder.PathDirection;
import org.tahomarobotics.robot.path.Waypoint;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.util.ChartData;

import java.util.List;

/**
 * Abstract path command that implements the basics of path following and provides common methods
 * needed for autonomous commanding.
 *
 */
public abstract class ChassisPathCommand extends Command {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChassisPathCommand.class);

    private final Chassis chassis = Chassis.getInstance();

    private final RobotState robotState = RobotState.getInstance();

    // used to help generate path
    private final PathBuilder pathBuilder;

    // list of way-points used for display purposes and path controllers
    private final List<Waypoint> waypoints;

    private final PathActions pathActions;

    protected final Mirror mirror;

    // final pose calculated from the initial pose and the ensuing path
    private final Pose2D finalPose;

    private final PathDirection direction;

    // motion limits
    public static final double MAX_ACCEL = 150;

    private static final double MOTION_TIMEOUT = 0.;

    // Motion Profiles generated from path
    private final MotionProfiles motionProfiles;

    // Motion state (current and set-point)
    private final MotionState motionSetpoint = new MotionState();
    private final MotionState currentMotionState = new MotionState();

    // Motion controller field and gain constants
    private static final double POSITION_TOLERANCE = 1.0;
    private static final double kP = 0.03;
    private static final double kV = 0.1;
    private static final double kI = 0.0;
    private final MotionController motionController = new MotionController(kP, kV, kI,
            ChassisConstants.kFFV, ChassisConstants.kFFA, POSITION_TOLERANCE);

    // data collection for graphing
    private final ChartData chartData = new ChartData("Path Motion", "Time (sec)", "Velocity (in/sec)",
            new String[] {"expected", "actual", "voltage", "expected-pos", "actual-pos", "rot-velocity", "rot-voltage"});

    // start time of command
    private double startTime;

    // motion profile is complete
    private boolean motionComplete = false;

    // Robot Pose
    private final Pose2D currentPose = new Pose2D();

    // Path controller
    private final AdaptivePurePursuitController pathController;

    // Rotational motion state and controller
    private final MotionState rotCurrentState = new MotionState();
    private final MotionState rotSetpoint = new MotionState();
    private final MotionController rotationalController = new MotionController(0, 0, 0,
            ChassisConstants.kFFV_ROT, ChassisConstants.kFFA_ROT, POSITION_TOLERANCE);

    public ChassisPathCommand(final PathDirection direction, final Mirror mirror, final Pose2D initialPose) {
        this(direction, mirror, initialPose, ChassisConstants.LOOKAHEAD_DISTANCE);
    }
    public ChassisPathCommand(final PathDirection direction, final Mirror mirror, final Pose2D initialPose, double lookahead) {
        this(direction, mirror, initialPose, lookahead, false);
    }
    public ChassisPathCommand(final PathDirection direction, final Mirror mirror, final Pose2D initialPose, boolean killDuringLastSection) {
        this(direction, mirror, initialPose, ChassisConstants.LOOKAHEAD_DISTANCE, killDuringLastSection);
    }
    /**
     * Create the base path command object starting with the initial pose, the robot drive
     * direction and if the paths coordinates should be mirrored relative to the center lines
     * of the field.
     *
     * @param direction - forward or revered robot driving
     * @param mirror - coordinate mirroring about the center lines of field
     * @param initialPose - starting robot pose
     */
    public ChassisPathCommand(final PathDirection direction, final Mirror mirror, final Pose2D initialPose, double lookahead, boolean killDuringLastSection) {
        requires(chassis);
        this.direction = direction;
        this.mirror = mirror;

        // create the path with the path builder
        pathBuilder = new PathBuilder(direction, mirror, initialPose);
        createPath(pathBuilder);
        finalPose = pathBuilder.getFinalPose();
        waypoints = pathBuilder.createWaypoints();
        pathActions = pathBuilder.getPathActions();

        // create motion profiles for forward travel using the path sections
        motionProfiles = Motion2DProfileFactory.createTrapezoidMotionProfile(pathBuilder.getSections(), MAX_ACCEL, 360);

        setTimeout(motionProfiles.getTotalDuration() + MOTION_TIMEOUT);

        pathController = new AdaptivePurePursuitController(waypoints, lookahead);
    }

    /**
     * Returns the final robot pose after the path is complete
     *
     * @return final robot pose (x, y, hdg)
     */
    public Pose2D getFinalPose() {
        return finalPose;
    }

    /**
     * List of way-points the represents the path.
     *
     * @return list of way-points (x, y)
     */
    public List<Waypoint> getWaypoints() {
        return waypoints;
    }

    /**
     * Path command implementation should use the provided path builder to construct the path.
     *
     * @param pathBuilder - to be used in construction of the path
     */
    protected abstract void createPath(PathBuilder pathBuilder);

    /**
     * Initialization of the command state at the start of running
     */
    @Override
    protected void initialize() {
        startTime = Timer.getFPGATimestamp();

        pathActions.resetPathActions();

        motionProfiles.reset();
        currentMotionState.setPosition(0).setVelocity(0);
        motionController.reset();
        chartData.clear();
        pathController.reset();
        rotSetpoint.setTime(0).setVelocity(0).setAcceleration(0);
    }

    /**
     * Periodic execution of the command
     */
    @Override
    protected void execute() {

        double elapsedTime = Timer.getFPGATimestamp() - startTime;

        // obtain current motion state from RobotState
        robotState.getRobotState(currentPose, currentMotionState, elapsedTime, direction == PathDirection.Reversed);

        // process path actions
        pathActions.processPathActions(currentMotionState.position);

        // update set-point
        motionComplete = motionProfiles.getSetpoint(elapsedTime, motionSetpoint);

        // update path controller
        double curvature = pathController.update(currentPose);
        currentMotionState.setPosition(pathController.getDistance());

        // update motion controller to get required applied voltage
        double forwardVoltage = motionController.update(elapsedTime, currentMotionState, motionSetpoint);
        double batteryVoltage = RobotController.getInputVoltage();
        double forwardPower = direction.sign * forwardVoltage / batteryVoltage;

        // angular velocity [1/inches] * [inches/second] = [radians/second]
        double angularVelocity = curvature * motionSetpoint.velocity;

        // calculate rotational set-point from angularVelocity
        rotSetpoint.setAcceleration((angularVelocity - rotSetpoint.velocity)/(elapsedTime - rotSetpoint.time));
        rotSetpoint.setVelocity(angularVelocity);
        rotSetpoint.setTime(elapsedTime);

        // update rotational motion controller
        // note:  feed-back gain are set to zero therefore rotCurrentState is not used in this controller
        double rotationalVoltage = rotationalController.update(elapsedTime, rotCurrentState, rotSetpoint);
        double rotationalPower = rotationalVoltage / batteryVoltage;

        // command the motion
        chassis.setPower(forwardPower, rotationalPower);

        // chart the data
        chartData.addData(new double[] {elapsedTime, motionSetpoint.velocity, currentMotionState.velocity, 10*forwardVoltage,
                motionSetpoint.position/5, currentMotionState.position/5, rotSetpoint.velocity, 10*rotationalVoltage} );
    }

    /**
     * Checks for completion which returns true if command has completed or timed-out
     */
    @Override
    protected boolean isFinished() {
        // Definitely wait for motion profile and path actions to be complete
        // but only wait the timeout period if controller is on target
        return motionComplete && pathActions.arePathActionsComplete() && (motionController.onTarget() || isTimedOut());
    }

    /**
     * Finalization of the command state at the end of running
     */
    @Override
    protected void end() {

        // stop motion
        chassis.setPower(0, 0);

        // send chart data to dash-board
        SmartDashboard.putRaw("PathMotionChartData", chartData.serialize());

        // log completion information
        LOGGER.info(getClass().getSimpleName() + " Path " + (isTimedOut() ? "Timedout" : "Completed Successfully"));
    }
}

package org.tahomarobotics.robot.chassis;

import org.tahomarobotics.robot.RobotConstants;
import org.tahomarobotics.robot.util.Constants;

public class ChassisConstants {


    // Transmission
    private static final double FIRST_STAGE = 50d/10d;
    private static final double SECOND_STAGE = 52d/24d;
    private static final double GEAR_RATIO  = FIRST_STAGE * SECOND_STAGE;
    private static final double EFFICIENCY = 1;

    //Pulse Per Inch count
    private static final double WHEEL_DIA = 6.; // inches
    public static final double INCHES_PER_REVOLUTOIN = WHEEL_DIA * Math.PI / GEAR_RATIO;
    public static final double INCHES_PER_TICK = INCHES_PER_REVOLUTOIN/2048.0;
    public static final double INCHES_PER_SECOND = INCHES_PER_TICK * 10.0;

    private static final int MOTOR_COUNT = 4;

    // Wheels
    private static final double WHEEL_DIAMETER = WHEEL_DIA * Constants.METERS_PER_INCH;
    private static final double WHEEL_RADIUS = WHEEL_DIAMETER / 2;

    // Robot Rigid Body
    private static final double MASS = 150 * Constants.KILOGRAM_PER_LBMASS; // kg
    private static final double TRACK_HALF_WIDTH = 29 / 2.0 * Constants.METERS_PER_INCH;

    private static final double MOMENT_OF_INTERIA = 20779.439
            * Constants.KILOGRAM_PER_LBMASS
            * Constants.METERS_PER_INCH
            * Constants.METERS_PER_INCH; // lbmass in^2 to kg m^2


    private static final double TORQUE_PER_VOLT = RobotConstants.kT * MOTOR_COUNT * GEAR_RATIO * EFFICIENCY / RobotConstants.RESISTANCE;

    // voltage needed per unit of velocity (inches/second)
    public static final double kFFV  = Constants.METERS_PER_INCH * GEAR_RATIO  / WHEEL_RADIUS  / RobotConstants.kV * 36./24.5 * 36./44. * 120/135. * 120/118. * 30/22.5;

    // voltage needed per unit of acceleration (inches/second^2)
    public static final double kFFA  = Constants.METERS_PER_INCH * MASS * WHEEL_RADIUS / TORQUE_PER_VOLT * 1.2;

    // additional voltage needed per unit of angular acceleration (radians/sec^2 -> volts)
    public static final double kFFV_ROT = TRACK_HALF_WIDTH * GEAR_RATIO / WHEEL_RADIUS / RobotConstants.kV * 180/80. * 180/190.;
    public static final double kFFA_ROT = MOMENT_OF_INTERIA / TRACK_HALF_WIDTH * WHEEL_RADIUS / TORQUE_PER_VOLT;

    // Path Controller and constants
    public static final double LOOKAHEAD_DISTANCE = 24.0;


}

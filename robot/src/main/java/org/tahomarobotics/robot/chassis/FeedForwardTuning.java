package org.tahomarobotics.robot.chassis;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.util.ChartData;

import static org.tahomarobotics.robot.chassis.ChassisConstants.kFFV;

public class FeedForwardTuning extends Command {
    private final Chassis chassis = Chassis.getInstance();

    private final ChartData chartData = new ChartData("Path Motion", "Time (sec)", "Velocity (in/sec)",
            new String[] {"velocity", "position"});

    double time;

    Pose2D currentPose = new Pose2D();

    MotionState currentMotionState = new MotionState();

    double dist = -20;
    double wantedTime = 1;
    public FeedForwardTuning(){
        requires(chassis);
    }

    @Override
    protected void initialize() {
        time = Timer.getFPGATimestamp();
    }

    double speed = dist / wantedTime;

    @Override
    protected boolean isFinished() {
        chassis.wanted = -180/wantedTime;
        double time = this.timeSinceInitialized();
        RobotState.getInstance().getRobotState(currentPose, currentMotionState, time, false);

//        chassis.setPower(speed * 12 * kFFV / RobotController.getBatteryVoltage(), 0.0);
        chassis.setPower(0.0/*speed*12*2*Math.PI * kFFV / RobotController.getBatteryVoltage()*/, -Math.toRadians(180/wantedTime) * ChassisConstants.kFFV_ROT / RobotController.getBatteryVoltage());

        chartData.addData(new double[] {Timer.getFPGATimestamp(), chassis.getForwardVel(), currentMotionState.position / 10});
        return time> wantedTime;
    }

    @Override
    protected void end() {
        SmartDashboard.putRaw("PathMotionChartData", chartData.serialize());
    }

}

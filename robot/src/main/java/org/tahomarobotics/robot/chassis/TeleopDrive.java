package org.tahomarobotics.robot.chassis;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.OI;

public class TeleopDrive extends Command {

    // larger sensitivity for rotation
    private static final double ROTATIONAL_SENSITIVITY = 1.5;

    // normal sensitivity for forward
    private static final double FORWARD_SENSITIVITY = 1.5;

    // used to command drive motors
    private final Chassis chassis = Chassis.getInstance();

    // used for throttle input
    private final OI oi = OI.getInstance();



    public TeleopDrive(){
        requires(chassis);
    }

    @Override
    protected boolean isFinished() {
        double leftThrottle = oi.getLeftThrottle();
        double rightThrottle = oi.getRightThrottle();

        // convert left and right controls to forward and rotate so that each can be
        // desensitized with different values
        double forwardPower = desensitizePowerBased((rightThrottle + leftThrottle) / 2, FORWARD_SENSITIVITY);
        double rotatePower = desensitizePowerBased((rightThrottle - leftThrottle) / 2, ROTATIONAL_SENSITIVITY);

        chassis.setPower(forwardPower, rotatePower * .8);

        return false;
    }

    private double desensitizePowerBased(double value, double power) {
        return Math.pow(Math.abs(value), power - 1) * value;
    }

}

package org.tahomarobotics.robot.shooter;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.UpdateIF;
import org.tahomarobotics.robot.util.GenericMotor;
import org.tahomarobotics.robot.util.GenericMotorBuilder;
import org.tahomarobotics.robot.util.MotorInfo;
import org.tahomarobotics.robot.util.LiveChartData;

import java.util.ArrayList;
import java.util.List;

public class Shooter extends Subsystem implements UpdateIF, MotorInfo.SubsystemMotors {

    private static final Shooter INSTANCE = new Shooter();
    private final TalonFX shooterMotorLeft = new TalonFX(RobotMap.SHOOTER_ONE);
    private final TalonFX shooterMotorRight = new TalonFX(RobotMap.SHOOTER_TWO);

    private final ArrayList<GenericMotor> genericMotors = GenericMotorBuilder.convertMotorList(new TalonFX[]{shooterMotorLeft, shooterMotorRight});

    private Shooter(){

        shooterMotorLeft.setInverted(false);
        shooterMotorRight.setInverted(true);


        shooterMotorLeft.getStatusFramePeriod(StatusFrame.Status_2_Feedback0, 20);
        shooterMotorRight.getStatusFramePeriod(StatusFrame.Status_2_Feedback0, 20);

        shooterMotorRight.setNeutralMode(NeutralMode.Coast);
        shooterMotorLeft.setNeutralMode(NeutralMode.Coast);
        SmartDashboard.putNumber("set wanted speed", 0.0);
    }

    public static Shooter getInstance() {
        return INSTANCE;
    }

    @Override
    protected void initDefaultCommand() {
        setDefaultCommand(new Shoot());
    }

    public double getSpeed(){
        return shooterMotorRight.getSelectedSensorVelocity() / ShooterConstants.NATIVE_PER_100_MS_TO_RPM;
    }

    private double wantedSpeed = 0.0;

    public void setWantedSpeed(double wantedSpeed){
        this.wantedSpeed = wantedSpeed;
    }
    public double getWantedSpeed(){
        return wantedSpeed;
    }

    boolean onTarget = false;
    public void onTarget(boolean onTarget){
        this.onTarget = onTarget;
    }
    public boolean isOnTarget(){
        return onTarget;
    }
    public void setPower(double power) {
        SmartDashboard.putNumber("power", power);
        shooterMotorLeft.set(TalonFXControlMode.PercentOutput, power);
        shooterMotorRight.set(TalonFXControlMode.PercentOutput, power);
    }

    private double wantedSped = 0.0;
    public void updateWantedSpeed(double speed){
        wantedSped = speed;
    }

    public double getWantedSped(){
        return wantedSped;
    }

    LiveChartData chartData = new LiveChartData("Shooter"," Time ( Sec)", "Velocity (in/sec)",
            new String[]{"actual-vel", "wanted-vel"});
    public void update(boolean enabled){
        chartData.updateData(new double[]{Timer.getFPGATimestamp(), getSpeed(), getWantedSped()});

        SmartDashboard.putNumber("speed-shooter", getSpeed());
        SmartDashboard.putNumber("wanted speed", getWantedSpeed());
    }

    @Override
    public List<GenericMotor> getMotorInfo() {
        return genericMotors;
    }

    @Override
    public String getSubsystemName() {
        return Shooter.class.getSimpleName();
    }
}

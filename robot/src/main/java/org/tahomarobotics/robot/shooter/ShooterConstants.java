package org.tahomarobotics.robot.shooter;

public class ShooterConstants {
    public static final double GEARBOX_RATIO = 20.0/40.0;

    public static final double NATIVE_PER_100_MS_TO_RPM = GEARBOX_RATIO * 2048.0 / 10.0 / 60.0;

    public static final double SPEC_VOLTAGE = 12.0;
    public static final double FREE_SPEED = 6380; // RPM
    public static final double FREE_ANGULAR_VELOCITY = FREE_SPEED * 2 * Math.PI / 60; // radian/sec
    public static final double FREE_CURRENT = 1.5; // Amps
    public static final double STALL_TORQUE = 4.69; // N-m
    public static final double STALL_CURRENT = 257; // Amps
    public static final double RESISTANCE = SPEC_VOLTAGE / STALL_CURRENT;
    public static final double kT = STALL_TORQUE / (STALL_CURRENT - FREE_CURRENT); // Nm/Amp
    public static final double kV = FREE_ANGULAR_VELOCITY / (SPEC_VOLTAGE - RESISTANCE * FREE_CURRENT); // rad/s per volt

    public static final double kFFV =  GEARBOX_RATIO / Math.toDegrees(kV) * 10 * 5./4. * 2./3. * 4/4.6 * 4/4.25 * 5/4.5;
}

package org.tahomarobotics.robot.shooter;

import edu.wpi.first.wpilibj.command.Command;

public class PrepShooter extends Command {

    private final Shooter shooter = Shooter.getInstance();

    private double speed;
    private boolean spinTo = false;

    public PrepShooter(double speed, boolean spinTo){
        this.speed = speed;
        this.spinTo = spinTo;
    }

    @Override
    protected boolean isFinished() {
        if(spinTo) {
            shooter.setWantedSpeed(speed);
        }else {
            shooter.setWantedSpeed(0.0);
        }
        return true;
    }

}

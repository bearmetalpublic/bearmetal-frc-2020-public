package org.tahomarobotics.robot.shooter;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.limelight.LimelightWrapper;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.state.RobotState;

import static org.tahomarobotics.robot.shooter.ShooterConstants.*;

public class Shoot extends Command {

    private final OI oi = OI.getInstance();
    private final Shooter shooter = Shooter.getInstance();

    private final double MAX_ACCEL = 2500;
    private double kP = 0.005;
    private double kV = 0.00;
    private double kI = 0.0;

    private final MotionController motionController = new MotionController(kP, kV, kI,
            kFFV,0, 200);

    private final MotionState motionSetpoint = new MotionState();
    private final MotionState currentMotionState = new MotionState();

    public Shoot(){
        requires(shooter);
    }

    private double initTime;
    @Override
    protected void initialize() {
        prevTime = Timer.getFPGATimestamp();
    }

    double prevSpeed = 0.0;
    double prevTime = 0.0;
    double wantedSpeed = 0.0;
    @Override
    protected boolean isFinished() {

        double time = Timer.getFPGATimestamp();
        double dt = time - prevTime;
        double distance = LimelightWrapper.getInstance().getDistanceSkew();
        double wanted = (4576.585 - 16.63679*distance + .1089252*distance*distance - .00006650363*distance*distance*distance);
        wanted = LimelightWrapper.getInstance().isTargetAvalible() && !RobotState.getInstance().outOfRange() ? wanted : 3000;
//        double wanted = SmartDashboard.getNumber("set wanted speed", 0.0);
        wantedSpeed = wantedSpeed < wanted ? (wantedSpeed + Math.min((wanted - wantedSpeed), MAX_ACCEL * dt)) : (wantedSpeed > wanted ? (wantedSpeed - Math.min(MAX_ACCEL * dt, wantedSpeed - wanted)) : wantedSpeed);
        shooter.updateWantedSpeed(wantedSpeed);

        boolean accel = wantedSpeed < wanted;
        double speed = shooter.getSpeed();
        motionSetpoint.setVelocity(wantedSpeed);
        motionSetpoint.setAcceleration(accel ? 0.0 : MAX_ACCEL);

        currentMotionState.setVelocity(speed);
        currentMotionState.setAcceleration((currentMotionState.velocity - prevSpeed) / (dt));
        prevSpeed = currentMotionState.velocity;
        prevTime = time;

        double power = motionController.updateVel(time, currentMotionState, motionSetpoint);

        double bangBangFudgeFactor = .95;
        double volts = speed <= wantedSpeed - 80 ? 12*bangBangFudgeFactor : power;

        double scaledPow = (accel ? power : volts) / RobotController.getBatteryVoltage();

        scaledPow = oi.isClimbPrepped() ? 0.0 : scaledPow;
        shooter.setPower(scaledPow);
        shooter.onTarget(motionController.onTarget());
        return false;
    }

    @Override
    protected void end() {
        wantedSpeed = 0.0;
        shooter.updateWantedSpeed(0.0);
    }
}

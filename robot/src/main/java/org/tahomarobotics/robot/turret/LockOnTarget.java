package org.tahomarobotics.robot.turret;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.limelight.LimelightWrapper;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfileFactory;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.util.ChartData;

public class LockOnTarget extends Command {

	private double MAX_VEL = 100;
	private double MAX_POWER = MAX_VEL * TurretConstants.kFFV;

	private final Turret turret = Turret.getInstance();
	private double kP = 0.08;
	private double kV = 0.0;
	private double kI = 0.03;

	private final MotionController motionController = new MotionController(kP, kV, kI,
			0.0, 0.0, 2.5);

	private final MotionState motionSetpoint = new MotionState();
	private final MotionState currentMotionState = new MotionState();

	private final ChartData chartData = new ChartData("Turret"," Time ( Sec)", "Velocity (in/sec)",
			new String[]{"expected-vel", "actual-vel", "voltage", "expected-pos", "actual-pos"});

	public LockOnTarget(){
		requires(turret);
	}


	@Override
	protected boolean isFinished() {

		double offset = LimelightWrapper.getInstance().getX();
		boolean isTargetAvalible = LimelightWrapper.getInstance().isTargetAvalible();
		offset = isTargetAvalible ? offset : turret.getPosition();
		double defaultAngle = turret.getWantedDefaultAngle();
		if(turret.getHoldAngle() || (!isTargetAvalible || RobotState.getInstance().outOfRange())){
			motionSetpoint.setPosition(defaultAngle);
			currentMotionState.setPosition(turret.getPosition());
		}else {
			motionSetpoint.setPosition(0.0);
			currentMotionState.setPosition(offset);
		}

		motionSetpoint.setVelocity(0.0);

		currentMotionState.setVelocity(turret.getVelocity());

		double power = motionController.update(Timer.getFPGATimestamp(), currentMotionState, motionSetpoint);
		power = Math.abs(power) > MAX_POWER ? MAX_POWER * Math.signum(power) : power;
		double scaledPow = power / RobotController.getBatteryVoltage();

		turret.setTurretLolAngle(motionController.onTarget());
		if(!turret.isHallEffectDetected()){
			double pow = OI.getInstance().getLeftManipThrottle() / 10.;
			turret.setPower(pow, true);
		}else {
			turret.setPower(scaledPow);
		}
		return false;
	}

	@Override
	protected void end() {
//		new Searching().start();
	}
}

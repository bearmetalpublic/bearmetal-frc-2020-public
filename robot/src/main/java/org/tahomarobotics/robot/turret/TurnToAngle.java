package org.tahomarobotics.robot.turret;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.path.ActionIF;

public class TurnToAngle extends Command implements ActionIF {

    private final Turret turret = Turret.getInstance();
    private final double angle;
    private final boolean forcePosition;
    private final boolean instantComplete;

    public TurnToAngle(double angle){
        this(angle, false);
    }

    public TurnToAngle(double angle, boolean forcePosition){
        this(angle, forcePosition, 5);
    }

    public TurnToAngle(double angle, boolean forcePosition, double timeout){
        this.forcePosition = forcePosition;
        this.angle = angle;
        if(timeout != -1.0) {
            instantComplete = false;
            setTimeout(timeout);
        } else {
            instantComplete = true;
        }
    }

    @Override
    protected void initialize() {
        turret.setWantedDefualtAngle(angle);
        turret.setHoldAngle(forcePosition);
        turret.addCommand();
    }

    @Override
    protected boolean isFinished() {
        return isTimedOut() || instantComplete;
    }

    @Override
    protected void end() {
        if(!instantComplete){
            if(turret.getRunningCommands() == 1) {
                turret.setWantedDefualtAngle(0.0);
                turret.setHoldAngle(false);
            }
        }
        turret.removeCommand();
    }
}

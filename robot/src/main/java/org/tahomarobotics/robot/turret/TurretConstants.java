package org.tahomarobotics.robot.turret;

import org.tahomarobotics.robot.util.Constants;

public class TurretConstants {

	public static final double OUT_OF_BOUNDS = 0.0;

	private static final double FIRST_STAGE = 60d/12d;
	private static final double SECOND_STAGE = 50/18d;
	private static final double THIRD_STAGE = 140d/36d;
	private static final double GEAR_RATIO  = FIRST_STAGE * SECOND_STAGE * THIRD_STAGE;
	public static final double ABS_GEAR_RATIO = THIRD_STAGE;
	public static final double DEGREES_PER_REV = 360d/GEAR_RATIO;

	//todo find these numbers
	public static final double TURRET_MASS = 15.9 * Constants.KILOGRAM_PER_LBMASS;
	public static final double TURRET_RADIUS = 2.533 * Constants.METERS_PER_INCH;
	private static final double TURRET_INTERIA = TURRET_MASS * TURRET_RADIUS * TURRET_RADIUS;

	public static final double SPEC_VOLTAGE = 12.0;
	public static final double FREE_SPEED = 11000; // RPM
	public static final double FREE_ANGULAR_VELOCITY = FREE_SPEED * 2 * Math.PI / 60; // radian/sec
	public static final double FREE_CURRENT = 1.4; // Amps
	public static final double STALL_TORQUE = 0.97; // N-m
	public static final double STALL_CURRENT = 100; // Amps
	public static final double RESISTANCE = SPEC_VOLTAGE / STALL_CURRENT;
	public static final double kT = STALL_TORQUE / (STALL_CURRENT - FREE_CURRENT); // Nm/Amp
	public static final double kV = FREE_ANGULAR_VELOCITY / (SPEC_VOLTAGE - RESISTANCE * FREE_CURRENT); // rad/s per volt

	public static final double kFFV =  GEAR_RATIO / Math.toDegrees(kV) * 27/20. * 182./200.;

//	private static final double FUDGE_FACTOR = 1.0 / Math.PI;
	public static final double kFFA =  TURRET_INTERIA * RESISTANCE / (GEAR_RATIO * kT) * Math.PI / 180;



}

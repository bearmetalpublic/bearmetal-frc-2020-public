package org.tahomarobotics.robot.turret;

import com.ctre.phoenix.sensors.CANCoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DigitalOutput;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.InitIF;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.UpdateIF;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.util.LiveChartData;
import org.tahomarobotics.robot.util.MathUtil;

import org.tahomarobotics.robot.limelight.LimelightWrapper;
import org.tahomarobotics.robot.util.GenericMotor;
import org.tahomarobotics.robot.util.GenericMotorBuilder;
import org.tahomarobotics.robot.util.MotorInfo;

import java.util.ArrayList;
import java.util.List;

public class Turret extends Subsystem implements UpdateIF, InitIF, MotorInfo.SubsystemMotors {

	private static final Turret INSTANCE = new Turret();

	private final CANSparkMax turret = new CANSparkMax(RobotMap.TURRET, CANSparkMaxLowLevel.MotorType.kBrushless);

	private final DigitalInput hallEffectSensor = new DigitalInput(0);

	private final double threshold = 10;
	private double offset = 0.0;//TODO find this number


	private final double MIN_POSE = -230;
	private final double MAX_POSE = 60;

	private int turnCommandsRunning = 0;

	private final ArrayList<GenericMotor> genericMotors = GenericMotorBuilder.convertMotorList(new CANSparkMax[]{turret});
	private boolean holdAngle;

	@Override
	public void init() {
		defaultAngle = 0.0;
		holdAngle = false;
	}

	public void setHoldAngle(boolean holdPosition) {
		this.holdAngle = holdPosition;
	}

	public boolean getHoldAngle(){
		return holdAngle;
	}

	public Turret(){
		turret.setInverted(true);
		turret.setIdleMode(CANSparkMax.IdleMode.kBrake);
	}

	public boolean atMin(double threshold){
		return getPosition() < MIN_POSE + threshold;
	}

	public boolean atMax(double threshold){
		return getPosition() > MAX_POSE - threshold;
	}

	public void setPositionAbsolute(){
		turret.getEncoder().setPosition(0.0);
	}

	public double getPosition(){
		return (turret.getEncoder().getPosition() * TurretConstants.DEGREES_PER_REV);
	}
	public double getVelocity(){
		return turret.getEncoder().getVelocity() * TurretConstants.DEGREES_PER_REV / 60.0;
	}

	private boolean hallEffectDetected = false;
	public void setHallEffectDetected(boolean hallEffectDetected){
		this.hallEffectDetected = hallEffectDetected;
		if(hallEffectDetected){
			setPositionAbsolute();
		}
	}

	public boolean isHallEffectDetected(){
		return hallEffectDetected;
	}

	public void setPower(double pow){
		setPower(pow, false);
	}
	public void setPower(double power, boolean override){
		if(!isHallEffectDetected() && !override) {
			power = 0;
		} else if(atMax(threshold) && power > 0){
			power = 0;
		}else if(atMin(threshold) && power < 0){
			power = 0;
		}
		SmartDashboard.putNumber("power turret", power);
		turret.set(power);
	}

	public static Turret getInstance(){
		return INSTANCE;
	}

	private double angle = 0.0;
	public void setWantedAngle(double angle){
		this.angle = angle;
	}
	public void addCommand(){
		turnCommandsRunning++;
	}
	public void removeCommand(){
		turnCommandsRunning--;
	}
	public int getRunningCommands(){
		return turnCommandsRunning;
	}
	public double getWantedAngle(){
		return angle;
	}


	boolean lol = false;
	public void setTurretLolAngle(boolean angle){
		lol = angle;
	}
	public boolean getLol(){
		return lol;
	}
	private double defaultAngle = 0.0;
	public void setWantedDefualtAngle(double angle){
		this.defaultAngle = angle;
	}
	public double getWantedDefaultAngle(){
		return defaultAngle;
	}
	@Override
	protected void initDefaultCommand() {
		setDefaultCommand(new LockOnTarget());
	}

	private LiveChartData chartData = new LiveChartData("Turret"," Time (Sec)", "Degrees",
			new String[]{"actual-vel", "actual-angle", "targetPosition"});
	public void update(boolean isEnabled) {
		SmartDashboard.putBoolean("sensor", hallEffectDetected);
		SmartDashboard.putBoolean("at min", atMin(threshold));
		SmartDashboard.putBoolean("at max", atMax(threshold));
		if(!hallEffectSensor.get() && !isHallEffectDetected()){
			setHallEffectDetected(true);
		}
		chartData.updateData(new double[]{Timer.getFPGATimestamp(), getVelocity(), getPosition(), getWantedAngle()});
	}

	@Override
	public List<GenericMotor> getMotorInfo() {
		return genericMotors;
	}

	@Override
	public String getSubsystemName() {
		return Turret.class.getSimpleName();
	}
}

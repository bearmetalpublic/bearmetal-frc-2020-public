/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.smile.SmileFactory;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LiveChartData {

	private final List<String> names = new ArrayList<>();
	private double[] data = new double[0];
	private final NetworkTable networkTable = NetworkTableInstance.getDefault().getTable("SmartDashboard");
	private final NetworkTableEntry liveCharts = networkTable.getEntry("LiveCharts");
	private NetworkTableEntry chartData = null;

	protected LiveChartData(List<String> names) {
		for (String name : names) {
			this.names.add(name);
		}
	}

	/**
	 * Create a ChartData object with the specified title, axis and series names.
	 *
	 * @param title - chart title
	 * @param xaxis - x axis title
	 * @param yaxis - y axis title
	 * @param seriesNames - series name array (array length must match add data length)
	 */
	public LiveChartData(String title, String xaxis, String yaxis, String[] seriesNames) {
		names.add(title);
		names.add(xaxis);
		names.add(yaxis);
		for (String name : seriesNames) {
			names.add(name);
		}
		chartData = networkTable.getEntry(title);
		String currentCharts = liveCharts.getString("");
		liveCharts.setString(currentCharts.equals("") ? title : currentCharts + ":" + title);
	}

	/**
	 * Adds a array of data for a new data point(s).  The first data point needs to be
	 * the x-axis followed by the y-axis value for each series.
	 *
	 * @param data - {x-axis, y-axis[0], y-axis[1], ..., y-axis[n-l]}
	 */
	public void updateData(double[] data) {
		assert(data != null && data.length == (names.size() - 2));
		this.data = data.clone();
		chartData.setRaw(serialize());
	}

	/**
	 * Serialize the titles and the data array into a byte array encoded as JSON
	 *
	 * @return byte array of serialized chart data
	 */
	public byte[] serialize() {

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectMapper mapper = new ObjectMapper(new SmileFactory());
		ObjectNode node = mapper.createObjectNode();
		node.putPOJO("names", names);
		node.putPOJO("data", data);

		try {
			mapper.writeValue(out, node);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return out.toByteArray();
	}

	/**
	 * De-serialize the titles and data array into this class.
	 *
	 * @param json - serialized byte array
	 * @return de-serialized LiveChartData object
	 */
	public static LiveChartData deserialize(byte[] json) {

		try {
			ObjectMapper mapper = new ObjectMapper(new SmileFactory());
			JsonNode root = mapper.readTree(json);
			JsonNode namesNode = root.findValue("names");
			if (!namesNode.isMissingNode() && namesNode.isArray()) {
				ObjectReader reader = mapper.readerFor(new TypeReference<List<String>>() {
				});
				List<String> list = reader.readValue(namesNode);
				LiveChartData liveChartData = new LiveChartData(list);

				JsonNode dataNode = root.findValue("data");
				if (!dataNode.isMissingNode() && dataNode.isArray()) {
					reader = mapper.readerFor(new TypeReference<List<double[]>>() {
					});
					liveChartData.setData(reader.readValue(dataNode));
					return liveChartData;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}


	public String getTitle() {
		return names.get(0);
	}
	public String getXAxis() {
		return names.get(1);
	}
	public String getYAxis() {
		return names.get(2);
	}
	public int getSeriesCount() {
		return names.size() - 3;
	}

	public String getSeriesName(int i) {
		return names.get(3+i);
	}

	public double[] getData() {
		return data;
	}

	private void setData(double[] data) {
		this.data = data.clone();
	}
}

package org.tahomarobotics.robot.util;

public interface GenericMotor {
	double getTempature();
	double getAmperage();
}

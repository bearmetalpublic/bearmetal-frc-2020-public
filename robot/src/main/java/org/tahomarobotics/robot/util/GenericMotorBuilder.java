/*
 * Copyright 2020 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.util;

import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.revrobotics.CANSparkMax;

import java.util.ArrayList;

public class GenericMotorBuilder {

	public static GenericMotor convertMotor(CANSparkMax sparkMax) {
		return new GenericSparkMax(sparkMax);
	}

	public static GenericMotor convertMotor(TalonFX talonFX){
		return new GenericTalonFX(talonFX);
	}

	public static GenericMotor convertMotor(TalonSRX talonSRX){
		return new GenericTalonSRX(talonSRX);
	}

	public static ArrayList<GenericMotor> convertMotorList(CANSparkMax[] sparkMaxArrayList){
		ArrayList<GenericMotor> genericMotors = new ArrayList<>();
		for(CANSparkMax sparkMax : sparkMaxArrayList){
			genericMotors.add(convertMotor(sparkMax));
		}
		return genericMotors;
	}

	public static ArrayList<GenericMotor> convertMotorList(TalonFX[] talonFXArrayList){
		ArrayList<GenericMotor> genericMotors = new ArrayList<>();
		for(TalonFX talonFX : talonFXArrayList){
			genericMotors.add(convertMotor(talonFX));
		}
		return genericMotors;
	}

	public static ArrayList<GenericMotor> convertMotorList(TalonSRX[] talonSRXArrayList){
		ArrayList<GenericMotor> genericMotors = new ArrayList<>();
		for(TalonSRX talonSRX : talonSRXArrayList){
			genericMotors.add(convertMotor(talonSRX));
		}
		return genericMotors;
	}

	private static class GenericSparkMax implements GenericMotor{
		private final CANSparkMax sparkMax;

		public GenericSparkMax(CANSparkMax sparkMax){
			this.sparkMax = sparkMax;
		}

		@Override
		public double getTempature() {
			return sparkMax.getMotorTemperature();
		}

		@Override
		public double getAmperage() {
			return sparkMax.getOutputCurrent();
		}
	}

	private static class GenericTalonFX implements GenericMotor{
		private final TalonFX talonFX;

		public GenericTalonFX(TalonFX talonFX){
			this.talonFX = talonFX;
		}

		@Override
		public double getTempature() {
			return talonFX.getTemperature();
		}

		@Override
		public double getAmperage() {
			return talonFX.getStatorCurrent();
		}
	}

	private static class GenericTalonSRX implements GenericMotor{

		private final TalonSRX talonSRX;

		public GenericTalonSRX(TalonSRX talonSRX){
			this.talonSRX = talonSRX;
		}

		@Override
		public double getTempature() {
			return talonSRX.getTemperature();
		}

		@Override
		public double getAmperage() {
			return talonSRX.getStatorCurrent();
		}
	}
}

package org.tahomarobotics.robot;

public class RobotMap {

    public static final int LEFT_REAR_MOTOR = 1;
    public static final int LEFT_FRONT_MOTOR = 2;
    public static final int RIGHT_REAR_MOTOR = 3;
    public static final int RIGHT_FRONT_MOTOR = 4;

    public static final int HOOD = 9;

    public static final int TURRET = 10;
    public static final int TURRET_ENCODER = 0;

    public static final int TRASH_COMPACTOR = 6;
    public static final int QUEUER_MOTOR_CONVEYOR = 11;
    public static final int QUEUER_SONIC_SENSOR_FIRST = 0;
    public static final int QUEUER_SONIC_SENSOR_SECOND = 1;

    public static final int SHOOTER_ONE = 7;
    public static final int SHOOTER_TWO = 12;

    public static final int PIGEON_IMU = 7;

    public static final int COLLECTOR_MOTOR_FRONT = 5;
    public static final int COLLECTOR_SOLENOID = 0;

    public static final int CLIMBER_MOTOR = 22;
    public static final int CLIMBER_MOTOR_1 = 23;
    public static final int CLIMBER_SOLENOID = 3;


}

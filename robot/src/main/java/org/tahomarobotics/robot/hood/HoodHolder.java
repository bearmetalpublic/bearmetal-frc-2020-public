package org.tahomarobotics.robot.hood;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.limelight.LimelightWrapper;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfileFactory;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.util.ChartData;
import org.tahomarobotics.robot.util.LiveChartData;

public class HoodHolder extends Command {

    private final Hood hood = Hood.getInstance();

    // Motion state (current and set-point)
    private final MotionState motionSetpoint = new MotionState();
    private final MotionState currentMotionState = new MotionState();

    // Motion controller field and gain constants
    private static final double POSITION_TOLERANCE = 1.0;
    private static final double kP = 0.2;
    private static final double kV = 0.0;
    private static final double kI = 0.09;
    private final MotionController motionController = new MotionController(kP, kV, kI,
            HoodConstants.kFFV, HoodConstants.kFFA, POSITION_TOLERANCE);

    private static final double MAX_SPEED = 15;

    private double startTime;
    public HoodHolder(){
        requires(hood);
    }

    @Override
    protected void initialize() {
        hood.setActive();

        startTime = Timer.getFPGATimestamp();

        motionSetpoint.setPosition(0).setAcceleration(0).setVelocity(0);

    }

    @Override
    protected boolean isFinished() {
        double time = Timer.getFPGATimestamp();

        double deltaTime = time - startTime;

        currentMotionState.setPosition(hood.getPosition());
        currentMotionState.setVelocity(hood.getSpeed());
        double distance = LimelightWrapper.getInstance().getDistanceSkew();
        double pos = 25.64304 + .2657268*distance - .002330573*distance*distance + .00000462028*distance*distance*distance;

        pos = pos > 37 ? 37 : (pos < 18 ? 18 : pos);

        pos = LimelightWrapper.getInstance().isTargetAvalible() && !RobotState.getInstance().outOfRange() ? pos : 37;

        hood.pos = pos;

        motionSetpoint.setPosition(pos);
        motionSetpoint.setVelocity(0.0);


        double voltage = motionController.update(deltaTime, currentMotionState, motionSetpoint);
        double percent = voltage / RobotController.getBatteryVoltage();
        percent = Math.abs(hood.getSpeed()) > MAX_SPEED ? Math.min(0.05, Math.abs(percent)) * Math.signum(percent) : percent;
        hood.setPower(-percent);

        return false;
    }

}

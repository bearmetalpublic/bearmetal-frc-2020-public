package org.tahomarobotics.robot.hood;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.InitIF;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.UpdateIF;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.util.GenericMotor;
import org.tahomarobotics.robot.util.GenericMotorBuilder;
import org.tahomarobotics.robot.util.LiveChartData;
import org.tahomarobotics.robot.util.MotorInfo;

import java.util.ArrayList;
import java.util.List;

public class Hood extends Subsystem implements UpdateIF, InitIF, MotorInfo.SubsystemMotors {

    private static final Hood INSTANCE = new Hood();

    private final CANSparkMax hood = new CANSparkMax(RobotMap.HOOD, CANSparkMaxLowLevel.MotorType.kBrushless);

    ArrayList<GenericMotor> motors = GenericMotorBuilder.convertMotorList(new CANSparkMax[]{hood});
    @Override
    public List<GenericMotor> getMotorInfo() {
        return motors;
    }

    @Override
    public String getSubsystemName() {
        return Hood.class.getSimpleName();
    }

    private enum HoodMode{
        Active,
        Hold
    }
    private HoodMode state = HoodMode.Hold;

    public Hood(){

        hood.setInverted(false);

        hood.setIdleMode(CANSparkMax.IdleMode.kBrake);

        hood.getEncoder().setPosition(0);

        hood.setSmartCurrentLimit(30);

    }

    public static Hood getInstance(){
        return INSTANCE;
    }

    public double getSpeed(){
        return hood.getEncoder().getVelocity() * HoodConstants.DEGREES_PER_REV / 60.0;
    }

    public double getPosition(){
        return 40-(hood.getEncoder().getPosition() * HoodConstants.DEGREES_PER_REV);
    }

    private double initTime = Double.POSITIVE_INFINITY;
    private static final double TIMEOUT = .5;
    public void setPower(double pow){
        pow = Timer.getFPGATimestamp() - initTime > TIMEOUT ? pow : 0.0;
        SmartDashboard.putNumber("pow", pow);
        hood.set(pow);
    }

    public void setActive(){
        state = HoodMode.Active;
    }

    public void zeroHood(){
        hood.getEncoder().setPosition(0);
    }

    @Override
    public void init() {
        System.out.println("init");
        SmartDashboard.putNumber("hood wanted pos", 38);
        initTime = Timer.getFPGATimestamp();
        setPower(0.0);
        new ZeroHood().start();
    }

    double pos = 0.0;
    public void setWantedPos(double pos){
        this.pos = pos;
    }
    public double getWantedPos(){
        return pos;
    }
    LiveChartData data = new LiveChartData("Hood"," Time ( Sec)", "Velocity (in/sec)",
            new String[]{"actual-vel", "actual-pos", "wanted-pos"});
    @Override
    public void update(boolean isEnabled) {
        double pos = getPosition();
        SmartDashboard.putNumber("pos", pos);
        data.updateData(new double[]{Timer.getFPGATimestamp(), getSpeed(), pos, getWantedPos()});
        if(pos > 40){
            zeroHood();
        }
    }

    @Override
    protected void initDefaultCommand() {
        setDefaultCommand(new HoodHolder());
    }
}

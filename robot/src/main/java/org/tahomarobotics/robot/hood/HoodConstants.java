package org.tahomarobotics.robot.hood;

import org.tahomarobotics.robot.util.Constants;

public class HoodConstants {
    private static final double FIRST_STAGE = 60d/12d;
    private static final double SECOND_STAGE = 350/24d;
    private static final double GEAR_RATIO  = FIRST_STAGE * SECOND_STAGE;

    public static final double MAX_ANGLE = 40;//degrees from vertical
    public static final double DEGREES_PER_REV = 360d/GEAR_RATIO;

    //todo find these numbers
    public static final double HOOD_MASS = 1 * Constants.KILOGRAM_PER_LBMASS;
    public static final double HOOD_RADIUS = 7.519107869 * Constants.METERS_PER_INCH;
    private static final double HOOD_INTERIA = HOOD_MASS * HOOD_RADIUS * HOOD_RADIUS;

    public static final double SPEC_VOLTAGE = 12.0;
    public static final double FREE_SPEED = 11000; // RPM
    public static final double FREE_ANGULAR_VELOCITY = FREE_SPEED * 2 * Math.PI / 60; // radian/sec
    public static final double FREE_CURRENT = 1.4; // Amps
    public static final double STALL_TORQUE = 0.97; // N-m
    public static final double STALL_CURRENT = 100; // Amps
    public static final double RESISTANCE = SPEC_VOLTAGE / STALL_CURRENT;
    public static final double kT = STALL_TORQUE / (STALL_CURRENT - FREE_CURRENT); // Nm/Amp
    public static final double kV = FREE_ANGULAR_VELOCITY / (SPEC_VOLTAGE - RESISTANCE * FREE_CURRENT); // rad/s per volt

    public static final double kFFV =  GEAR_RATIO / Math.toDegrees(kV) * 3;

    public static final double kFFA =  HOOD_INTERIA * RESISTANCE / (GEAR_RATIO * kT) * Math.PI / 180 * 2.5;



}

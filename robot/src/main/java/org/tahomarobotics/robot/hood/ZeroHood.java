package org.tahomarobotics.robot.hood;

import edu.wpi.first.wpilibj.command.Command;

public class ZeroHood extends Command {

    private final Hood hood = Hood.getInstance();

    private final double THRESHOLD = 20;

    public ZeroHood(){
        requires(hood);
    }

    @Override
    protected void initialize() {
        hood.setActive();
        setTimeout(1.5);
    }

    @Override
    protected boolean isFinished() {
        hood.setPower(-0.15);
        return isTimedOut();
    }

}

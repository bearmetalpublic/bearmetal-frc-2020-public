/*
 * Copyright 2020 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Scheduler;
import org.tahomarobotics.robot.auto.Autonomoose;

import org.tahomarobotics.robot.chassis.Chassis;
import org.tahomarobotics.robot.collector.Collector;
import org.tahomarobotics.robot.collector.Collector;
import org.tahomarobotics.robot.hood.Hood;
import org.tahomarobotics.robot.hood.ZeroHood;
import org.tahomarobotics.robot.chassis.FeedForwardTuning;
import org.tahomarobotics.robot.limelight.LimelightWrapper;
import org.tahomarobotics.robot.limelight.LimelightWrapper;
import org.tahomarobotics.robot.queuer.Queuer;
import org.tahomarobotics.robot.shooter.Shooter;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.turret.LockOnTarget;
import org.tahomarobotics.robot.turret.Turret;
import org.tahomarobotics.robot.util.LiveChartData;
import org.tahomarobotics.robot.util.MotorInfo;

import java.util.ArrayList;
import java.util.List;

public class Robot extends TimedRobot {

    private final List<UpdateIF> updates = new ArrayList<>();
    private final List<InitIF> inits = new ArrayList<>();
    private final List<DisabledInitIF> disabledInits = new ArrayList<>();

    private final MotorInfo motorInfo = new MotorInfo();
    private final RobotState robotState = RobotState.getInstance();

    private final LimelightWrapper limelight = LimelightWrapper.getInstance();
    private final Autonomoose autonomous = Autonomoose.getInstance();
    private final Hood hood = Hood.getInstance();
    private final OI oi = OI.getInstance();

    private boolean inited = false;

    private double disabledTime = 0.0;
    private final static double DISABLED_RESET_TIME = 5;

    @Override
    public void robotInit() {

        inits.add(oi);
        inits.add(autonomous);
        inits.add(Chassis.getInstance());
        inits.add(hood);
        inits.add(Turret.getInstance());
        inits.add(limelight);

        updates.add(oi);
        updates.add(Chassis.getInstance());
        updates.add(Turret.getInstance());
        updates.add(Collector.getInstance());
        updates.add(Shooter.getInstance());
        updates.add(limelight);
        updates.add(robotState);
        updates.add(autonomous);
        updates.add(hood);
        updates.add(Queuer.getInstance());

        disabledInits.add(Chassis.getInstance());
        disabledInits.add(Collector.getInstance());

        motorInfo.addSubsystem(Chassis.getInstance());
        motorInfo.addSubsystem(Queuer.getInstance());
        motorInfo.addSubsystem(Shooter.getInstance());
        motorInfo.addSubsystem(Turret.getInstance());
        motorInfo.addSubsystem(Collector.getInstance());
        motorInfo.addSubsystem(hood);

        motorInfo.start();
    }

    @Override
    public void robotPeriodic() {
        boolean isEnabled = DriverStation.getInstance().isEnabled();
        for (UpdateIF subsystem : updates) {
            subsystem.update(isEnabled);
        }
        Scheduler.getInstance().run();
    }

    //bean was here
    @Override
    public void autonomousInit() {
        if(!inited){
            inited = true;
            for(InitIF init : inits){
                init.init();
            }
        }
        autonomous.start();
    }

    @Override
    public void teleopInit() {
        if(!inited){
            inited = true;
            for(InitIF init : inits){
                init.init();
            }
        }
    }

    @Override
    public void disabledInit() {
        disabledTime = Timer.getFPGATimestamp();
    }

    @Override
    public void disabledPeriodic() {
        if(Timer.getFPGATimestamp() - disabledTime >= DISABLED_RESET_TIME && inited){
            inited = false;
            for(DisabledInitIF disabledInit : disabledInits){
                disabledInit.disabledInit();
            }
        }
    }

    public static void main(String[] args) {
        Robot.startRobot(Robot::new);
    }
}

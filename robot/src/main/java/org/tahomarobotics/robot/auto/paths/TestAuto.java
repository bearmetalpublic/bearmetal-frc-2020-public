package org.tahomarobotics.robot.auto.paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.chassis.ChassisPathCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.Pose2D;

public class TestAuto extends ChassisPathCommandGroup {
	private static final Logger LOGGER = LoggerFactory.getLogger(TestAuto.class);

	public static final double MAX_SPEED = 100;

	private static final Pose2D initialPose = new Pose2D(40, 40, 0.0);

	private final Pose2D startingPose;

	public TestAuto(PathBuilder.Mirror mirror){
		startingPose = PathBuilder.mirrorPose2D(initialPose, mirror);

		ChassisPathCommand prior;
		addPath(prior = new DriveForward(mirror, initialPose));
		//addPath(new LorgCorcile(mirror, prior.getFinalPose()));
	}

	private class DriveForward extends ChassisPathCommand {
		public DriveForward(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(100, MAX_SPEED);
			pathBuilder.addArc(90,50,MAX_SPEED);

		}
	}
	private class LorgCorcile extends ChassisPathCommand {
		public LorgCorcile(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArc(360, 20, MAX_SPEED);
		}
	}

	@Override
	public Pose2D getStartingPose() {
		return startingPose;
	}
}

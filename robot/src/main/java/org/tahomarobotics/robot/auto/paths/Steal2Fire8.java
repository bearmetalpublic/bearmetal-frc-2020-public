package org.tahomarobotics.robot.auto.paths;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.auto.commands.ShootingCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.collector.CollectorAutoCommand;
import org.tahomarobotics.robot.collector.CollectorSolenoidCommand;
import org.tahomarobotics.robot.path.PathActions;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.turret.TurnToAngle;

public class Steal2Fire8 extends ChassisPathCommandGroup {

	private static final Logger LOGGER = LoggerFactory.getLogger(MiddleLeft.class);

	public static final double MAX_SPEED = 120;

	private static final Pose2D initialPose = new Pose2D(136.75 + 3.5, 297, 0.0);

	private final Pose2D startingPose;

	public Steal2Fire8(PathBuilder.Mirror mirror){
		startingPose = PathBuilder.mirrorPose2D(initialPose, mirror);

		ChassisPathCommand prior;
		addParallel(new CollectorAutoCommand(15));
		addPath(prior = new GrabTwo(mirror, initialPose));
		addPath(prior = new Shoot5(mirror, prior.getFinalPose()));
		addPath(prior = new GrabTwoPart2(mirror, prior.getFinalPose()));
		addPath(prior = new Grab1(mirror, prior.getFinalPose()));
		addPath(prior = new Grab1Part2(mirror, prior.getFinalPose()));
		addPath(prior = new Shoot5Part2(mirror, prior.getFinalPose()));

	}

	private class GrabTwo extends ChassisPathCommand {
		public GrabTwo(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(81.25 + 2, MAX_SPEED * .8, new PathActions.PathAction(new CollectorSolenoidCommand(DoubleSolenoid.Value.kReverse), 0.0, false));
		}
	}

	private class Shoot5 extends ChassisPathCommand {
		public Shoot5(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(8 + 6.5, MAX_SPEED * .6);
			pathBuilder.addArcToPoint(171.,260, MAX_SPEED *.6, new PathActions.PathAction(
					new TurnToAngle(-85, false), 0.0, false));
			pathBuilder.addLine(165.5, MAX_SPEED * .95,
					new PathActions.PathAction(new ShootingCommand(), .9, true));
		}
	}

	private class GrabTwoPart2 extends ChassisPathCommand {
		public GrabTwoPart2(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(60.375, MAX_SPEED * .75);
			pathBuilder.addArcToPoint(216,179, MAX_SPEED * .5);
			pathBuilder.addLine(15, MAX_SPEED * .5);
		}
	}

	private class Grab1 extends ChassisPathCommand {
		public Grab1(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(15, MAX_SPEED * .5);
			pathBuilder.addArcToPoint(206,190, MAX_SPEED * .5);
		}
	}

	private class Grab1Part2 extends ChassisPathCommand {
		public Grab1Part2(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(222,160, MAX_SPEED * .5);
			pathBuilder.addLine(5, MAX_SPEED * .5);
		}
	}

	private class Shoot5Part2 extends ChassisPathCommand {
		public Shoot5Part2(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(5, MAX_SPEED * .6);
			pathBuilder.addArcToPoint(168.453,147.881, MAX_SPEED * .6,
					new PathActions.PathAction(new TurnToAngle(-110, false), .0, false));
			pathBuilder.addLine(51.47, MAX_SPEED * .8,
					new PathActions.PathAction(new ShootingCommand(), .7, true));
		}
	}

	@Override
	public Pose2D getStartingPose() {
		return startingPose;
	}
}

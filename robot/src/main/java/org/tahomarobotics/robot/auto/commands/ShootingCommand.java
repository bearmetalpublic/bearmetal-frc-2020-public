package org.tahomarobotics.robot.auto.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.tahomarobotics.robot.path.ActionIF;
import org.tahomarobotics.robot.queuer.Eject;
import org.tahomarobotics.robot.shooter.PrepShooter;
import org.tahomarobotics.robot.turret.TurnToAngle;
import org.tahomarobotics.robot.turret.Turret;

public class ShootingCommand extends CommandGroup implements ActionIF {

    public ShootingCommand(){
        this(.75);
    }
    public ShootingCommand(double timeout){
        addSequential(new TurretGood());
        addSequential(new ShooterGood());
        addSequential(new Eject(timeout));
    }

    @Override
    protected void end() {
        Turret.getInstance().setWantedAngle(0.0);
    }
}

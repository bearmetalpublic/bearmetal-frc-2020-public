package org.tahomarobotics.robot.auto.paths;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.auto.commands.ShootingCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.collector.CollectorAutoCommand;
import org.tahomarobotics.robot.collector.CollectorSolenoidCommand;
import org.tahomarobotics.robot.path.ActionIF;
import org.tahomarobotics.robot.path.PathActions;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.turret.TurnToAngle;

public class Steal2Fire10 extends ChassisPathCommandGroup {

	private static final Logger LOGGER = LoggerFactory.getLogger(MiddleLeft.class);

	public static final double MAX_SPEED = 130;

	private static final Pose2D initialPose = new Pose2D(136.75 + 3.5, 297, 0.0);

	private final Pose2D startingPose;

	public Steal2Fire10(PathBuilder.Mirror mirror){
		startingPose = PathBuilder.mirrorPose2D(initialPose, mirror);

		ChassisPathCommand prior;
		addParallel(new CollectorAutoCommand(15));
		addPath(prior = new GrabTwo(mirror, initialPose));
		addPath(prior = new Shoot5(mirror, prior.getFinalPose()));
		addPath(prior = new Shoot5Lol(mirror, prior.getFinalPose()));
		addPath(prior = new GrabTwoPart2(mirror, prior.getFinalPose()));
		addPath(prior = new Grab3Part1(mirror, prior.getFinalPose()));
		addPath(prior = new Grab3Part2(mirror, prior.getFinalPose()));
		addPath(prior = new Shoot5Part2(mirror, prior.getFinalPose()));

	}

	private class GrabTwo extends ChassisPathCommand {
		public GrabTwo(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(81.25 + 2, MAX_SPEED * .8, new PathActions.PathAction(new CollectorSolenoidCommand(DoubleSolenoid.Value.kReverse), 0.0, false));
		}
	}

	private class Shoot5 extends ChassisPathCommand {
		public Shoot5(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(8 + 6.5, MAX_SPEED * .6);
			pathBuilder.addArcToPoint(171.,260, MAX_SPEED *.6, new PathActions.PathAction(
					new TurnToAngle(-85, false), 0.0, false));
			pathBuilder.addLine(165.5, MAX_SPEED * .95,
					new PathActions.PathAction(new ShootingCommand(), .9, true));
		}
	}
	private class Shoot5Lol extends ChassisPathCommand {
		public Shoot5Lol(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(140,35, MAX_SPEED * .9);
		}
	}

	private class GrabTwoPart2 extends ChassisPathCommand {
		public GrabTwoPart2(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(190,60, MAX_SPEED * .75);
			pathBuilder.addArcToPoint(240,102, MAX_SPEED * .75);
		}
	}

	private class Grab3Part1 extends ChassisPathCommand {
		public Grab3Part1(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(186,95, MAX_SPEED * .75);
		}
	}

	private class Grab3Part2 extends ChassisPathCommand {
		public Grab3Part2(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(207,65, MAX_SPEED * .75);
			pathBuilder.addArcToPoint(265, 30, MAX_SPEED * .75);
			pathBuilder.addLine(24, MAX_SPEED);
		}
	}

	private class Shoot5Part2 extends ChassisPathCommand {
		public Shoot5Part2(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(225,67, MAX_SPEED * .8,
					new PathActions.PathAction(new TurnToAngle(-160, false), .0, false));
			pathBuilder.addArcToPoint(177,90, MAX_SPEED * .8,
					new PathActions.PathAction(new ShootingCommand(), .7, true));
		}
	}

	@Override
	public Pose2D getStartingPose() {
		return startingPose;
	}
}

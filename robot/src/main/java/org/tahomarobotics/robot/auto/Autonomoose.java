/*
 * Copyright 2020 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.auto;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.InitIF;
import org.tahomarobotics.robot.UpdateIF;
import org.tahomarobotics.robot.auto.paths.*;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.path.PathBuilder.Mirror;

import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.state.RobotState;

/**
 * The Autonomous class is responsible for selection of the autonomous command
 * and then starting and stopping it during play.
 *
 */
public class Autonomoose implements UpdateIF, InitIF {

    private static final Logger LOGGER = LoggerFactory.getLogger(Autonomoose.class);

    private enum StartPosition {
        LEFT,
        RIGHT
    }

    private enum AutoSelections {
        TEST,
        STEAL_2_FIRE_10,
        STEAL_2_FIRE_8,
        LEFT_GRAB_2_GRAB_3,
        LEFT_GRAB_2_GRAB_5
    }

    private static final Autonomoose INSTANCE = new Autonomoose();

    private final RobotState robotState = RobotState.getInstance();

    private ChassisPathCommandGroup selectedAutonomousPath;
    private AutoSelections autoSelection;
    private Mirror mirror = Mirror.None;

    private final SendableChooser<AutoSelections> chooser = new SendableChooser<>();
    private final SendableChooser<StartPosition> posChooser = new SendableChooser<>();

    private Autonomoose() {

        chooser.setDefaultOption(AutoSelections.TEST.toString(), AutoSelections.TEST);
        chooser.addOption(AutoSelections.STEAL_2_FIRE_10.toString(), AutoSelections.STEAL_2_FIRE_10);
        chooser.addOption(AutoSelections.STEAL_2_FIRE_8.toString(), AutoSelections.STEAL_2_FIRE_8);
        chooser.addOption(AutoSelections.LEFT_GRAB_2_GRAB_3.toString(), AutoSelections.LEFT_GRAB_2_GRAB_3);
        chooser.addOption(AutoSelections.LEFT_GRAB_2_GRAB_5.toString(), AutoSelections.LEFT_GRAB_2_GRAB_5);

        posChooser.setDefaultOption("Left", StartPosition.LEFT);
        posChooser.addOption("Right", StartPosition.RIGHT);

        SmartDashboard.putData("PathChooser", chooser);
        SmartDashboard.putData("Position", posChooser);
    }

    /**
     * Returns the Autonomous singleton
     */
    public static Autonomoose getInstance() {
        return INSTANCE;
    }

    /**
     * Start running the selected autonomous command.
     */
    public void start() {
        if (selectedAutonomousPath != null) {
            selectedAutonomousPath.publish();
            selectedAutonomousPath.start();
        }
    }

    /**
     * Stop running the selected autonomous command.
     */
    public void stop() {
        if (selectedAutonomousPath != null) {
            selectedAutonomousPath.cancel();
        }
    }

    @Override
    public void init(){
    }

    @Override
    public void update(boolean isEnabled) {

        Mirror mirror = getMirror();
        AutoSelections autoSelection = chooser.getSelected();

        if (mirror == this.mirror && autoSelection == this.autoSelection) {
            return;
        }

        this.mirror = mirror;
        this.autoSelection = autoSelection;

        LOGGER.info(DriverStation.getInstance().getAlliance().toString() + " " + autoSelection + " " + mirror);

        switch(autoSelection){
            case TEST:
                selectedAutonomousPath = new TestAuto(mirror);
                break;
            case STEAL_2_FIRE_10:
                selectedAutonomousPath = new Steal2Fire10(mirror);
                break;
            case STEAL_2_FIRE_8:
                selectedAutonomousPath = new Steal2Fire8(mirror);
                break;
            case LEFT_GRAB_2_GRAB_3:
                selectedAutonomousPath = new LeftGrab2Grab3(mirror);
                break;
            case LEFT_GRAB_2_GRAB_5:
                selectedAutonomousPath = new LeftGrab2Grab5(mirror);
                break;
        }

        selectedAutonomousPath.publish();

        Pose2D startingPose = selectedAutonomousPath.getStartingPose();
        robotState.resetRobotPose(startingPose.x, startingPose.y, startingPose.heading);
    }

    private Mirror getMirror() {
        DriverStation.Alliance alliance = DriverStation.getInstance().getAlliance();
        StartPosition startPosition = posChooser.getSelected();

        return alliance == DriverStation.Alliance.Blue ?
                (startPosition == StartPosition.LEFT ? Mirror.Y :  Mirror.None) :
                (startPosition == StartPosition.LEFT ? Mirror.X : Mirror.Both);
    }
}

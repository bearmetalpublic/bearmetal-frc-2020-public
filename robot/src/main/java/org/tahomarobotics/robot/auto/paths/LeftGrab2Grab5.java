package org.tahomarobotics.robot.auto.paths;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.auto.commands.ShootingCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.collector.CollectorAutoCommand;
import org.tahomarobotics.robot.collector.CollectorSolenoidCommand;
import org.tahomarobotics.robot.path.PathActions;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.turret.TurnToAngle;

public class LeftGrab2Grab5 extends ChassisPathCommandGroup {

	private static final Logger LOGGER = LoggerFactory.getLogger(LeftGrab2Grab5.class);

	public static final double MAX_SPEED = 120;

	private static final Pose2D initialPose = new Pose2D(138.678, 27.75, 0.0);

	private final Pose2D startingPose;

	public LeftGrab2Grab5(PathBuilder.Mirror mirror){
		startingPose = PathBuilder.mirrorPose2D(initialPose, mirror);

		ChassisPathCommand prior;
		addParallel(new CollectorAutoCommand(15));
		addPath(prior = new Grab2(mirror, initialPose));
		addPath(prior = new Shoot5(mirror, prior.getFinalPose()));
		addPath(prior = new Grab5(mirror, prior.getFinalPose()));
		addPath(prior = new Shoot5Part2(mirror, prior.getFinalPose()));

	}

	private class Grab2 extends ChassisPathCommand {
		public Grab2(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(110, MAX_SPEED, new PathActions.PathAction(
					new CollectorSolenoidCommand(DoubleSolenoid.Value.kReverse), 0.0, false)
					, new PathActions.PathAction(new TurnToAngle(-190, true, 3.5), 0.0, false));
		}
	}

	private class Shoot5 extends ChassisPathCommand {
		public Shoot5(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(188, 72, MAX_SPEED * .6);
			pathBuilder.addArcToPoint(150,100, MAX_SPEED * .6,
					new PathActions.PathAction(new ShootingCommand(2), .8, true));

		}
	}

	private class Grab5 extends ChassisPathCommand {
		public Grab5(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(200,90, MAX_SPEED * .7);
			pathBuilder.addArcToPoint(250,108, MAX_SPEED * .4);
			pathBuilder.addArcToPoint(267,114, MAX_SPEED * .4
					, new PathActions.PathAction(new TurnToAngle(-190, false)
							, 0.2, false));
			pathBuilder.addArcToPoint(322,70, MAX_SPEED * .4);
			pathBuilder.addLine(15 + 10, MAX_SPEED * .4);
		}
	}

	private class Shoot5Part2 extends ChassisPathCommand {
		public Shoot5Part2(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(10, MAX_SPEED * .5);
			pathBuilder.addArcToPoint(290.392, 80.346, MAX_SPEED * .5);
			pathBuilder.addLine(140.7, MAX_SPEED * .8,
					new PathActions.PathAction(new ShootingCommand(), .9, true));
		}
	}


	@Override
	public Pose2D getStartingPose() {
		return startingPose;
	}
}

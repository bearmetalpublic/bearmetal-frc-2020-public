package org.tahomarobotics.robot.auto.paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.chassis.ChassisPathCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.Pose2D;

public class MiddleLeft extends ChassisPathCommandGroup {
	private static final Logger LOGGER = LoggerFactory.getLogger(MiddleLeft.class);

	public static final double MAX_SPEED = 100;

	private static final Pose2D initialPose = new Pose2D(120.531, 94.655, 0.0);

	private final Pose2D startingPose;

	public MiddleLeft(PathBuilder.Mirror mirror){
		startingPose = PathBuilder.mirrorPose2D(initialPose, mirror);

		ChassisPathCommand prior;
		addPath(prior = new GrabTwo(mirror, initialPose));
		addPath(prior = new ShootFirstFive(mirror, prior.getFinalPose()));
		addPath(prior = new Grab3Balls(mirror, prior.getFinalPose()));
		addPath(prior = new Shoot3(mirror, prior.getFinalPose()));
	}

	private class GrabTwo extends ChassisPathCommand {
		public GrabTwo(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(178.119,85.981,MAX_SPEED);
			pathBuilder.addArcToPoint(232.864, 100.305, MAX_SPEED);

		}
	}
	private class ShootFirstFive extends ChassisPathCommand {
		public ShootFirstFive(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(189, 91, MAX_SPEED);
			pathBuilder.addArcToPoint(170, 94.655, MAX_SPEED);
		}
	}

	private class Grab3Balls extends ChassisPathCommand {
		public Grab3Balls(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(218, 68, MAX_SPEED);
			pathBuilder.addArcToPoint(228.575, 50.531, MAX_SPEED);
			pathBuilder.addArcToPoint(272, 26.5, MAX_SPEED);
			pathBuilder.addLine(26, MAX_SPEED);
		}
	}

	private class Shoot3 extends ChassisPathCommand {
		public Shoot3(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(208, 94.655, MAX_SPEED);
		}
	}

	@Override
	public Pose2D getStartingPose() {
		return startingPose;
	}
}

package org.tahomarobotics.robot.auto.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.turret.Turret;

public class TurretGood extends Command {

    private final Turret turret = Turret.getInstance();

    private int add = 0;
    @Override
    protected boolean isFinished() {
        add += turret.getLol() ? 1 : -add;
        return add > 5;
    }
}

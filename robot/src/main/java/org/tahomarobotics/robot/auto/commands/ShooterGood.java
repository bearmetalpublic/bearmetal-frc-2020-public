package org.tahomarobotics.robot.auto.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.shooter.Shooter;
import org.tahomarobotics.robot.turret.Turret;

public class ShooterGood extends Command {

    private final Shooter shooter = Shooter.getInstance();

    private int add = 0;

    @Override
    protected boolean isFinished() {
        add += shooter.isOnTarget() ? 1 : -add;
        return add > 5;
    }
}

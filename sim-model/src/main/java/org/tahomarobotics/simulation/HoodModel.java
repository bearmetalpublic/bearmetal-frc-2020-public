/*
 * Copyright 2020 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.simulation;

import org.tahomarobotics.sim.model.Encoder;
import org.tahomarobotics.sim.model.Motor;
import org.tahomarobotics.sim.model.SimRobot;
import org.tahomarobotics.sim.model.Transmission;

public class HoodModel {

    private static final double FIRST_STAGE = 36d/12d;
    private static final double SECOND_STAGE = 350/24d;
    private static final double GEAR_RATIO  = FIRST_STAGE * SECOND_STAGE;

    private final Motor hoodMotor = Motor.createNeo500();
    private final Encoder hoodEncoder = new Encoder();

    private final Transmission hoodDrive = new Transmission(new Motor[] { hoodMotor },
            false, GEAR_RATIO, GEAR_RATIO);

    public HoodModel() {
    }

    public void initialize(SimRobot sim) {
        sim.registerMotor(SimModelRobotMap.HOOD, hoodMotor);
        sim.registerEncoder(SimModelRobotMap.HOOD, hoodEncoder);
    }

    public void update(double time, double dT) {
        hoodDrive.update();
    }
}

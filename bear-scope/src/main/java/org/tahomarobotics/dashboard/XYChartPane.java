/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.dashboard;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import org.tahomarobotics.robot.util.ChartData;

import java.util.ArrayList;
import java.util.List;


public class XYChartPane extends AnchorPane implements InstrumentView {

    private final List<ChartData> chartDataCollection = new ArrayList<>();
    private int selectedChartDataIndex;

    private final NumberAxis xAxis = new NumberAxis();
    private final NumberAxis yAxis = new NumberAxis();
    private final LineChart<Number, Number> chart = new LineChart<>(xAxis, yAxis);
    private final Text text = new Text();
    public final ToolBar toolBar;

    public XYChartPane() {
        chart.setCreateSymbols(false);

        Button left = new Button("<--");
        left.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                shift(-1);
            }
        });
        Button right = new Button("-->");
        right.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                shift(1);
            }
        });

        toolBar = new ToolBar();
        toolBar.getItems().addAll(left, right, text);

        AnchorPane.setTopAnchor(chart, 0.0);
        AnchorPane.setLeftAnchor(chart, 0.0);
        AnchorPane.setRightAnchor(chart, 0.0);
        AnchorPane.setBottomAnchor(chart, 0.0);

        chart.setAnimated(false);
        getChildren().add(chart);
        updateText();
    }

    private void shift(int i) {
        if (chartDataCollection.isEmpty()) {
            return;
        }
        selectedChartDataIndex += i;
        selectedChartDataIndex = Math.min(Math.max(selectedChartDataIndex, 0), chartDataCollection.size() - 1);
        System.out.println("shift = " + selectedChartDataIndex);
        updateChart(chartDataCollection.get(selectedChartDataIndex));
        updateText();
    }

    private void updateText(){
        text.setText(String.format("Profile: %d of %d", selectedChartDataIndex + 1, chartDataCollection.size()));
    }

    @Override
    public Node getView() {
        return this;
    }

    @Override
    public void updateData(long currentNanoTime) {
    }

    public void update(final ChartData chartData) {
        chartDataCollection.add(chartData);
        selectedChartDataIndex = chartDataCollection.size() - 1;
        updateChart(chartData);
        updateText();
    }

    private void updateChart(final ChartData chartData) {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                int seriesCount = chartData.getSeriesCount();

                chart.setTitle(chartData.getTitle());
                xAxis.setLabel(chartData.getXAxis());
                yAxis.setLabel(chartData.getYAxis());

                chart.getData().clear();

                for (int i = 0; i < seriesCount; i++) {
                    ObservableList<Data<Number, Number>> seriesData = FXCollections.<Data<Number, Number>>observableArrayList();

                    for(double[] data : chartData.getData()) {
                        seriesData.add(new XYChart.Data<Number, Number>(data[0], data[i+1]));
                    }
                    Series<Number, Number> series = new XYChart.Series<Number, Number>(chartData.getSeriesName(i), seriesData);
                    chart.getData().add(series);
                }
            }
        });

    }

}

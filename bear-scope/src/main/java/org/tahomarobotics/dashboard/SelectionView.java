package org.tahomarobotics.dashboard;

import edu.wpi.first.networktables.*;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class SelectionView extends HBox {

    private static final Logger LOGGER = LoggerFactory.getLogger(SelectionView.class);

    public SelectionView(String... keys){
        NetworkTable table = NetworkTableInstance.getDefault().getTable("SmartDashboard");

        for(String key : keys){
            NetworkTable subTable = table.getSubTable(key);
            autoEntry entry = new autoEntry(subTable, key);

            VBox vertBox = new VBox();

            Platform.runLater(() -> {
                //Text showing the name of the Chooser
                Text text = new Text(key);

                //Align the text so it is in the center
                HBox horizBox = new HBox();
                horizBox.getChildren().add(text);
                horizBox.setAlignment(Pos.BASELINE_CENTER);

                //Put the text on top of the comboBox
                vertBox.getChildren().add(horizBox);
                vertBox.getChildren().add(entry);

                getChildren().add(vertBox);
            });
        }
    }

    private class autoEntry extends ComboBox<String>{
        /*
            Because each Chooser has it's own table, it has different entries which correspond
            to the data you are trying to get. For example, KEY_ACTIVE returns the key needed
            to get the current active selection, whether that be a selected option OR the default
         */
        private static final String KEY_DEFAULT = "default";
        private static final String KEY_SELECTED = "selected";
        private static final String KEY_OPTIONS = "options";
        private static final String KEY_ACTIVE = "active";

        private String[] options;
        private String name;

        private final NetworkTable table;

        /**
         * Class which stores the network table and listens for changing values on the table
         * @param networkTable NetworkTable which contains chooser
         */
        public autoEntry(NetworkTable networkTable, String key) {
            table = networkTable;
            this.name = key;

            valueProperty().addListener(this::changed);
            table.addEntryListener(this::entryListener, EntryListenerFlags.kUpdate | EntryListenerFlags.kImmediate | EntryListenerFlags.kNew);
        }

        private void entryListener(NetworkTable table, String key, NetworkTableEntry entry, NetworkTableValue value, int i){
            switch(key){
                case KEY_OPTIONS:
                    String[] newOptions = value.getStringArray();
                    if(!Arrays.equals(newOptions, options)){
                        options = newOptions;
                        Platform.runLater(() -> setItems(FXCollections.observableArrayList(options)));
                        LOGGER.info(String.format("Selection: %s - Options updated - %d options", this.name, newOptions.length));
                    }

                    break;
                case KEY_ACTIVE:
                    Platform.runLater(() -> setValue(entry.getString("")));
                    LOGGER.info(String.format("Selection: %s - Active option updated - %s", this.name, entry.getString("")));
                    break;
            }
        }

        /**
         * Listener for changes in the combo box, listening for user selection
         * @param ov
         * @param t
         * @param t1 Selected Value
         */
        private void changed(ObservableValue ov, String t, String t1){
            LOGGER.info(String.format("Selection: %s - Local Change - %s", this.name, t1));
            table.getEntry(KEY_SELECTED).setString(t1);
        }
    }
}

package org.tahomarobotics.dashboard.livechart;

import edu.wpi.first.networktables.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import org.tahomarobotics.dashboard.InstrumentView;
import org.tahomarobotics.dashboard.livechart.LiveChartData;

import java.util.ArrayList;

public class LiveChartView extends VBox implements InstrumentView {
	private NetworkTable table = NetworkTableInstance.getDefault().getTable("SmartDashboard");
	private NetworkTableEntry liveChartsTable = table.getEntry("LiveCharts");
	private String lastCharts = "";

	private final ArrayList<LiveChartData> liveCharts = new ArrayList<>();
	private LiveChartData selectedChart = null;

	private ToolBar toolBar = new ToolBar();

	private boolean init = false;

	private final Button pauseButton;
	private final Button resetButton;

	public LiveChartView(){
		getChildren().add(toolBar);

		resetButton = new Button("Reset");
		resetButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				resetCharts();
			}
		});
		pauseButton = new Button("Pause");
		pauseButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				pauseSwap();
			}
		});
		toolBar.getItems().add(resetButton);
		toolBar.getItems().add(pauseButton);

		setFillWidth(true);
	}

	private void pauseSwap() {
		if(selectedChart.getPaused()){
			selectedChart.setPaused(false);
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					selectedChart.setBackground(new Background(new BackgroundFill(new Color(0.95686275, 0.95686275, 0.95686275, 1), CornerRadii.EMPTY, Insets.EMPTY)));
				}
			});
		} else {
			selectedChart.setPaused(true);
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					selectedChart.setBackground(new Background(new BackgroundFill(new Color(0.9019607843, 0.26666666667, 0.26666666667, 1), CornerRadii.EMPTY, Insets.EMPTY)));
				}
			});
		}
	}

	private void resetCharts(){
		for(LiveChartData chartData : liveCharts){
			chartData.reset();
		}
	}

	public void setSelectedChart(LiveChartData selectedChart) {
		if(this.selectedChart != null) {
			getChildren().remove(this.selectedChart);
			selectedChart.setPaused(false);
		}
		this.selectedChart = selectedChart;
		getChildren().add(selectedChart);
		VBox.setVgrow(selectedChart, Priority.ALWAYS);
	}

	@Override
	public Node getView() {
		return this;
	}

	private long lastNanotime = 0;
	@Override
	public void updateData(long currentNanoTime) {
		if(!init){
			init = true;
			getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent event) {
					switch (event.getCode()){
						case R:
							resetCharts();
							break;
						case P:
							pauseSwap();
							break;
					}
				}
			});
		}
		if(!liveChartsTable.getString("").equals(lastCharts)){
			lastCharts = liveChartsTable.getString("");
			String[] chartList = lastCharts.split(":");
			for(int i = 0; i < chartList.length; i++){
				if(i >= liveCharts.size()){
					//System.out.println("added chart: " + chartList[i]);
					LiveChartData chart = new LiveChartData(chartList[i]);
					liveCharts.add(chart);
					if(selectedChart == null){
						setSelectedChart(chart);
					}

					Button button = new Button(chart.getTitle());
					button.setOnAction(new EventHandler<ActionEvent>() {
						private final LiveChartData myChart = chart;
						@Override
						public void handle(ActionEvent event) {
							setSelectedChart(myChart);
						}
					});
					toolBar.getItems().add(button);
				}
			}
		}
		if(currentNanoTime - lastNanotime > 500000000){
//			for(LiveChartData chartData : liveCharts){
//				chartData.updateChart();
//			}
		}
	}
}

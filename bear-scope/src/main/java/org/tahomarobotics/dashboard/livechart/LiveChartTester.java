package org.tahomarobotics.dashboard.livechart;

import edu.wpi.first.networktables.EntryListenerFlags;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

public class LiveChartTester {
	private static final NetworkTable table = NetworkTableInstance.getDefault().getTable("SmartDashboard");
	private static final NetworkTableEntry entry = table.getEntry("test");

	public static void main(String[] args){
		LiveChartView view = new LiveChartView();
		table.getEntry("LiveCharts").setString("test");
		view.updateData(0);
		int i = 0;
		System.out.println(table.getEntry("LiveCharts").getString("oop"));
		while(true){
			view.updateData(i);
			if(i % 1000 == 0) {
				entry.setString("Ya Yeet: " + i);
			}
			i++;
		}
	}
}

/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.dashboard.livechart;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.smile.SmileFactory;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LiveChartDataFrame {

	private final List<String> names = new ArrayList<>();
	private double[] data = new double[0];

	protected LiveChartDataFrame(List<String> names) {
		for (String name : names) {
			this.names.add(name);
		}
	}

	/**
	 * De-serialize the titles and data array into this class.
	 *
	 * @param json - serialized byte array
	 * @return de-serialized LiveChartData object
	 */
	public static LiveChartDataFrame deserialize(byte[] json) {

		try {
			ObjectMapper mapper = new ObjectMapper(new SmileFactory());
			JsonNode root = mapper.readTree(json);
			JsonNode namesNode = root.findValue("names");
			if (!namesNode.isMissingNode() && namesNode.isArray()) {
				ObjectReader reader = mapper.readerFor(new TypeReference<List<String>>() {
				});
				List<String> list = reader.readValue(namesNode);
				LiveChartDataFrame liveChartDataFrame = new LiveChartDataFrame(list);

				JsonNode dataNode = root.findValue("data");
				if (!dataNode.isMissingNode() && dataNode.isArray()) {
					reader = mapper.readerFor(new TypeReference<double[]>() {
					});
					liveChartDataFrame.setData(reader.readValue(dataNode));
					return liveChartDataFrame;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}


	public String getTitle() {
		return names.get(0);
	}
	public String getXAxis() {
		return names.get(1);
	}
	public String getYAxis() {
		return names.get(2);
	}
	public int getSeriesCount() {
		return names.size() - 3;
	}

	public String getSeriesName(int i) {
		return names.get(3+i);
	}

	public double[] getData() {
		return data;
	}

	private void setData(double[] data) {
		this.data = data.clone();
	}
}

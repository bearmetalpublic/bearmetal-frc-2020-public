package org.tahomarobotics.dashboard.livechart;

import edu.wpi.first.networktables.*;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.tahomarobotics.dashboard.XYChartPane;

import java.util.ArrayList;
import java.util.function.Consumer;

public class LiveChartData extends VBox implements Consumer<EntryNotification> {
	private final NetworkTable table = NetworkTableInstance.getDefault().getTable("SmartDashboard");
	private final NetworkTableEntry chartData;

	private final int storedFrames = 150;

	private final String title;

	private String lastData;
	private boolean paused = false;

	private final ArrayList<LiveChartDataFrame> frames = new ArrayList<>();

	private final LiveXYChartPane chartPane;

	public LiveChartData(String title) {

		chartPane = new LiveXYChartPane(title);
		getChildren().add(chartPane);
		setFillWidth(true);

		VBox.setVgrow(chartPane, Priority.ALWAYS);

		this.title = title;
		System.out.println(title);
		chartData = table.getEntry(title);
		chartData.addListener(this, EntryListenerFlags.kNew | EntryListenerFlags.kUpdate | EntryListenerFlags.kLocal);
	}

	@Override
	public void accept(EntryNotification entryNotification) {
		byte[] rawData = entryNotification.getEntry().getRaw(null);
		//System.out.println(rawData);
		if(rawData != null) {
			LiveChartDataFrame newFrame = LiveChartDataFrame.deserialize(rawData);
			frames.add(newFrame);
			if (frames.size() > storedFrames) {
				frames.remove(0);
			}
			if(!paused) {
				updateChart();
			}
		}
	}

	public void updateChart(){
		chartPane.update(frames);
	}

	public String getTitle() {
		return title;
	}

	public void reset() {
		frames.clear();
		chartPane.clear();
	}

	public void setPaused(boolean paused){
		this.paused = paused;
	}

	public boolean getPaused(){
		return paused;
	}
}

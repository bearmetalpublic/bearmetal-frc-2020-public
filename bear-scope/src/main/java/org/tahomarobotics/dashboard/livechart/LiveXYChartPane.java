/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.dashboard.livechart;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import org.tahomarobotics.dashboard.InstrumentView;
import org.tahomarobotics.robot.util.ChartData;

import java.util.ArrayList;
import java.util.List;


public class LiveXYChartPane extends AnchorPane implements InstrumentView {

    private final List<ChartData> chartDataCollection = new ArrayList<>();
    private int selectedChartDataIndex;

    private final NumberAxis xAxis = new NumberAxis();
    private final NumberAxis yAxis = new NumberAxis();
    private final LineChart<Number, Number> chart = new LineChart<>(xAxis, yAxis);

    private final String title;

    private boolean chartExists = false;

    public LiveXYChartPane(String title) {
        chart.setCreateSymbols(false);

        this.title = title;

        AnchorPane.setTopAnchor(chart, 0.0);
        AnchorPane.setLeftAnchor(chart, 0.0);
        AnchorPane.setRightAnchor(chart, 0.0);
        AnchorPane.setBottomAnchor(chart, 0.0);

        chart.setAnimated(false);
        getChildren().add(chart);
    }

    @Override
    public Node getView() {
        return this;
    }

    @Override
    public void updateData(long currentNanoTime) {
    }

    public void update(final ArrayList<LiveChartDataFrame> chartData) {
        if(chartExists){
            updateChart(chartData);
        } else {
            chartExists = true;
            setUpChart(chartData);
        }
    }

    private void setUpChart(final ArrayList<LiveChartDataFrame> chartData) {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                chart.setTitle(title);
                xAxis.setLabel(chartData.get(0).getXAxis());
                xAxis.setForceZeroInRange(false);
                xAxis.setAutoRanging(true);
                yAxis.setLabel(chartData.get(0).getYAxis());
                //yAxis.setAutoRanging(true);

                chart.getData().clear();

                for(int i = 0; i < chartData.get(0).getSeriesCount(); i++){
                    ObservableList<Data<Number, Number>> seriesData = FXCollections.<Data<Number, Number>>observableArrayList();
                    for(LiveChartDataFrame data : chartData){
                        seriesData.add(new Data<Number, Number>(data.getData()[0], data.getData()[i+1]));
                    }
                    Series<Number, Number> series = new Series<Number, Number>(chartData.get(0).getSeriesName(i), seriesData);
                    chart.getData().add(series);
                }

//                for (int i = 0; i < chartData.get(0).getSeriesCount(); i++) {
//                    ObservableList<Data<Number, Number>> seriesData = FXCollections.<Data<Number, Number>>observableArrayList();
//
//                    for(double[] data : chartData.get(0).getData()) {
//                        seriesData.add(new Data<Number, Number>(data[0], data[i+1]));
//                    }
//                    Series<Number, Number> series = new Series<Number, Number>(chartData.get(0).getSeriesName(i), seriesData);
//                    chart.getData().add(series);
//                }
            }
        });
    }

    private void updateChart(ArrayList<LiveChartDataFrame> chartData){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                chart.getData().clear();
                for(int i = 0; i < chartData.get(0).getSeriesCount(); i++){
                    ObservableList<Data<Number, Number>> seriesData = FXCollections.<Data<Number, Number>>observableArrayList();
                    for(LiveChartDataFrame data : chartData){
                        seriesData.add(new Data<Number, Number>(data.getData()[0], data.getData()[i+1]));
                    }
                    Series<Number, Number> series = new Series<Number, Number>(chartData.get(0).getSeriesName(i), seriesData);
                    chart.getData().add(series);
                }
            }
        });
    }

    public void clear() {
        chart.getData().clear();
    }
}
